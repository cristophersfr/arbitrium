#!/usr/bin/python3
# socket_echo_client_dgram.py
import argparse
import socket
import struct
import random
import time
import math

# Define the packet header
HEADER_SIZE = 6  # Size of the header in bytes
PACKET_SIZE = 4096  # Size of the packet to be sent
OFFSET = PACKET_SIZE - HEADER_SIZE  # Size of the zero bytes packet to be sent

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

SCALING = 3 * 100 * 1024 # scaling factor

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process the args')
    parser.add_argument('-sip', '--src_ip', help='Insert the source IP', type=str)
    parser.add_argument('-dip', '--dst_ip', help='Insert the destination IP', type=str)
    parser.add_argument('-dport', '--dst_port', help='Insert the destination port', type=int)
    parser.add_argument('-a', '--amount', help='Amount of traffic to transmit', type=float)
    parser.add_argument('-t', '--time', help='Duration of transmission', type=int)
    parser.add_argument('-l', '--list', nargs='+', help='<Required> Set flag', required=True)
    args = parser.parse_args()

    if args.time:
        rounds = args.time
    else:
        rounds = 100

    src_ip = args.src_ip
    dst_ip = args.dst_ip
    dst_port = args.dst_port
    traffic_list = args.list

    flow_id = 0

    for i in range(len(traffic_list)):
        flow_amount = float(traffic_list[i])
        duration = args.time
        num_packets = math.trunc(flow_amount / PACKET_SIZE)

        # Define the server address
        server_address = (dst_ip, dst_port)
        for k in range(rounds):
            # Sending the packets
            for pkt_num in range(num_packets):
                # Pack the data to a structured byte
                data = struct.pack('!HL',
                                   flow_id,
                                   pkt_num)

                zeros = [0 for i in range(OFFSET)]
                byte_zeros = bytearray(zeros)
                raw = data + byte_zeros

                timestamp = time.time()
                sent = sock.sendto(raw, server_address)

                print('{0} sent_to {1} {2} {3} {4}'.format(
                    src_ip,
                    dst_ip,
                    flow_id,
                    pkt_num,
                    timestamp
                ))

            flow_id += 1

            # Wait for another round
            time.sleep(1)

        # overflow = num_packets % math.trunc(num_packets)

        # Calculate the size of the last packet to be sent
        # overflow * PACKET_SIZE

    sock.close()
