import threading
import requests
import datetime
import time
import math

from config import ONOS_IP, ONOS_PORT

BASE_URL_API = 'http://{0}:{1}/onos/v1'.format(ONOS_IP, ONOS_PORT)


class APIMonitor(object):
    def __init__(self, output):
        self.username = 'onos'
        self.password = 'rocks'
        self.output = output

    def get_stats(self):
        res = requests.get(
            BASE_URL_API + '/statistics/delta/ports',
            auth=(self.username, self.password)
        )
        timestamp = int(time.time())
        data = res.json()
        for stats in data['statistics']:
            device = stats["device"]
            device = device[(len(device) - 2):]

            for stat in stats['ports']:
                port = stat["port"]
                value = (stat["bytesReceived"] + stat["bytesSent"]) / stat["durationSec"]
                self.output.write("{0} {1} {2} {3}\n".format(timestamp, device, port, value))

    def get_max_utilization(self):
        res = requests.get(
            BASE_URL_API + '/statistics/delta/ports',
            auth=(self.username, self.password)
        )

        data = res.json()

        max_utilization = 0

        for stats in data['statistics']:
            for stat in stats['ports']:
                bytes_received = math.ceil(stat["bytesReceived"]/stat["durationSec"])
                bytes_sent = math.ceil(stat["bytesSent"]/stat["durationSec"])

                utilization = (bytes_sent + bytes_received)/2
                max_utilization = max(max_utilization, utilization)

        return max_utilization


class MonitoringThread(threading.Thread):
    def __init__(self, monitor):
        self._stopevent = threading.Event()
        self.monitor = monitor
        threading.Thread.__init__(self)

    def run(self):
        while self._stopevent.is_set() is False:
            time.sleep(30)
            self.monitor.get_stats()

    def stop(self):
        self._stopevent.set()
