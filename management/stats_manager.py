import os, time
import datetime
import logging
import copy
import math

from config import *
from utils import json_get_req


class StatsManager(object):
    def __init__(self, verbose=VERBOSE):
        self.last_stat = {}
        self.tm_store = []
        self.verbose = verbose
        # self.db_client = InfluxDBClient('localhost', 8086, 'onos', 'onos', 'onos')
        # self.db_client.create_database('network')

        os.environ['TZ'] = 'America/Maceio'
        time.tzset()

    @staticmethod
    def bitrate(old_stat, current_stat):
        delta_bytes = current_stat['bytes'] - old_stat['bytes']
        delta_time = current_stat['life'] - old_stat['life']
        return 1.0 * 8 * delta_bytes / delta_time if delta_time > 0 and delta_bytes >= 0 else None

    # TODO: Store this data also into InfluxDB.
    def add_stats(self, stat_list):
        tm = dict()
        filtered_stats = dict()

        # For a same flow we might receive many flow stats (possibly one for each switch) with different "life" values:
        # in some cases the differences are due to potential ONOS asynchrony in receiving stats event from a given
        # switch, in other cases a big difference might be due to a re-routing (some flow rules might persist, others
        # might be added as brand new with a 0-valued life).
        # Currently we are keeping the with biggest "life" value: in case of coexsistence of old+new rules this means
        # keeping the oldest, in case of all new rules this means the most updated.
        for app_stat in stat_list:
            for intents in app_stat['intents']:
                for intentKey, stats in intents.items():
                    for stat in stats:
                        flow_id = (intentKey, app_stat['id'], app_stat['name'])
                        if flow_id not in filtered_stats or \
                                (flow_id in filtered_stats and stat['life'] > filtered_stats[flow_id]['life']):
                            filtered_stats[flow_id] = stat

        # For each flow we check if a stat related to a previous polling round is available
        for flow_id, stat in filtered_stats.items():
            if flow_id in self.last_stat:
                bitrate = self.bitrate(self.last_stat[flow_id], stat)
                if bitrate is not None:
                    tm[flow_id] = bitrate
            self.last_stat[flow_id] = stat

        self.tm_store.append(tm)
        # if self.verbose:
        #     pprint({flow_id: bps_to_human_string(tm[flow_id]) for flow_id in tm})

    # Stores the data from the PORT_STATS into InfluxDB.
    def store_mlu(self, port_stats):
        for stats in port_stats:
            util_per_device = []
            utilization = {
                "measurement": "utilization",
                "tags": {
                    "device": stats["device"],
                },
                "time": datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": {
                    "value": 0
                }
            }

            for stat in stats['ports']:
                util_per_port = copy.deepcopy(utilization)
                util_per_port["tags"]["port"] = stat["port"]
                util_per_port["fields"]["value"] = util_per_port["fields"]["value"] + \
                                               math.ceil(stat["bytesReceived"]/stat["durationSec"])
                util_per_port["fields"]["value"] = util_per_port["fields"]["value"] + \
                                               math.ceil(stat["bytesSent"]/stat["durationSec"])

                util_per_device.append(util_per_port)

            # print(util_per_device)
            self.db_client.write_points(util_per_device, time_precision='ms')

    def poll_stats(self):
        logging.info("Polling Traffic Matrices...")
        intent_reply = json_get_req('http://{0}:{1}/onos/v1/imr/imr/intentStats'.format(ONOS_IP, ONOS_PORT))
        port_reply = json_get_req('http://{0}:{1}/onos/v1/statistics/delta/ports'.format(ONOS_IP, ONOS_PORT))
        if 'statistics' not in intent_reply:
            logging.info('IMR Stats returned empty')
            return
        if 'statistics' not in port_reply:
            logging.info('Port Stats returned empty')
            return
        self.add_stats(intent_reply['statistics'])
        # self.store_mlu(port_reply['statistics'])

    def get_tm_store(self):
        return self.tm_store
