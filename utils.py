import logging
import requests
from config import *

# logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(message)s')

def json_get_req(url):
    try:
        request = requests.get(url, auth=(ONOS_USER, ONOS_PASS))
        return request.json()
    except IOError as e:
        logging.error(e)
        return ''


def json_post_req(url, json_data):
    try:
        request = requests.post(url, json_data, auth=(ONOS_USER, ONOS_PASS))
        return request.json()
    except IOError as e:
        logging.error(e)
        return ''


def bps_to_human_string(value, to_byte_per_second=False):
    if to_byte_per_second:
        value = value/8.0
        suffix = 'B/s'
    else:
        suffix = 'bps'

    for unit in ['', 'K', 'M', 'G']:
        if abs(value) < 1000.0:
            return '%3.1f %s%s' % (value, unit, suffix)
        value /= 1000.0
    return '%.1f %s%s' % (value, 'T', suffix)
