#!/usr/bin/env python

# placement of profile roles among physical hosts
PLACEMENT = "sequential"


def seq_placement(num_hosts, num_edge_switches):
    return range(num_hosts)


def get_placement(num_hosts, num_edge_switches):
    return PLACEMENT_TYPES[PLACEMENT](num_hosts, num_edge_switches)


PLACEMENT_TYPES = {"sequential": seq_placement}
