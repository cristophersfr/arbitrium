from datacenter.placement import *
from datacenter.profiles import IperfProfile, PingProfile

class Datacenter(object):
    def __init__(self, num_edge_switches=2):
        self.num_edge_switches = num_edge_switches
        self.vlans = 0
        self.vlan_map = {}
        self.tenants = []

    def get_vlan(self, hostname):
        if hostname in self.vlan_map.keys():
            return self.vlan_map[hostname]
        else:
            return ":".join("%s" % i for i in range(self.vlans))

    def assign(self, hosts, tenant, indices):
        h = [hosts[x] for x in indices]
        tenant.create(h)
        for i in h:
            self.vlan_map[i.name] = tenant.vlan

    def setup(self, hosts, tenants):
        for t in tenants:
            t.assign_vlan(self.vlans)
            self.vlans += 1
            self.tenants.append(t)

        # TODO: Implement the placement strategies
        p = get_placement(len(hosts), self.num_edge_switches)
        index = 0

        for t in self.tenants:
            self.assign(hosts, t, p[index : index + t.num_nodes])
            index += t.num_nodes

        self.print_assignments()
        self.log_assignments()

    def print_assignments(self):
        for t in self.tenants:
            role_str = ", ".join(["{0}={1}".format(h.host.name, h)
                                  for h in t.apps])

            host_str = ", ".join([h.name for h in t.nodes])
            print("VLAN {0}: {1} - [{2}]".format(t.vlan, t, host_str))
            print("     ",format(role_str))

    def log_assignments(self):
        # TODO: Define the logging strategy
        pass

    def update(self, bw=None, outfile=None):
        # TODO: Improve the class polymorphims into this function.
        for tenant in self.tenants:
            if isinstance(tenant, IperfProfile):
                tenant.update(bw)
            if isinstance(tenant, PingProfile):
                tenant.update(outfile)


    def start(self):
        for tenant in self.tenants:
            tenant.start()

    def stop(self):
        for tenant in self.tenants:
            tenant.stop()
