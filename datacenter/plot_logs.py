import os
import pprint
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

results = []

files = os.listdir("./logs/")
files.sort()

strategies = ['mlu','sp', 'mc']
bws = [6.5, 7, 7.5, 8]
its = range(0, 60)
# pprint.pprint(files)
for strategy in strategies:
    for bw in bws:
        for i in its:
            filename = '{0}_bw_{1}_it_{2}.log'.format(strategy, str(bw), str(i))
            f = open("./logs/{0}".format(filename), "r")

            data = {
                'src_ip': [],
                'dst_ip': [],
                'latency': []
            }

            for line in f:
                l = line.split(" ")
                data['src_ip'].append(l[0])
                data['dst_ip'].append(l[1])
                if l[2] != '0':
                    data['latency'].append(float(l[3]))
                else:
                    data['latency'].append('lost')

            df = pd.DataFrame(data=data)

            # Remove self-loops
            df_no_loops = df[(df['src_ip'] != df['dst_ip'])]
            df_no_loops = df_no_loops.iloc[:, 1:3]

            non_lost_packets = df_no_loops[(df_no_loops.latency != 'lost')]

            # non_lost_packets = non_lost_packets[
            #     np.abs(non_lost_packets.latency - non_lost_packets.latency.mean()) <= (3 * non_lost_packets.latency.std())]

            lost_packets = df_no_loops[(df_no_loops.latency == 'lost')]

            metrics = {
                'strategy': strategy,
                'traffic': bw,
                'latency': round(non_lost_packets.latency.mean(), 2),
                'packet_loss': round(len(lost_packets) / len(df_no_loops), 2)
            }

            results.append(metrics)

df = pd.DataFrame(data=results)

results = {
    'strategy': [],
    'latency_mean': [],
    'latency_std': [],
    'packet_loss_mean': [],
    'packet_loss_std': []
}

for bw in bws:
    bw_df = df[(df.traffic == bw)]
    for strategy in strategies:
        new_df = bw_df[(bw_df.strategy == strategy)]

        new_df = new_df[
            np.abs(new_df.latency - new_df.latency.mean()) <= (3 * new_df.latency.std())]

        results['strategy'].append(strategy)
        results['latency_mean'].append(new_df.latency.mean())
        results['latency_std'].append(new_df.latency.std())
        results['packet_loss_mean'].append(new_df.packet_loss.mean())
        results['packet_loss_std'].append(new_df.packet_loss.std())

final_result = pd.DataFrame(data=results)

labels = bws
x = np.arange(len(bws))

mlu = final_result[(final_result.strategy == 'mlu')]
sp = final_result[(final_result.strategy == 'sp')]
mc = final_result[(final_result.strategy == 'mc')]

fig, ax = plt.subplots()
plt.errorbar(x=labels, y=sp.latency_mean, yerr=sp.latency_std, fmt='o--', label='SP')
plt.errorbar(x=labels, y=mlu.latency_mean, yerr=mlu.latency_std, fmt='o--', label='MLU')
plt.errorbar(x=labels, y=mc.latency_mean, yerr=mc.latency_std, fmt='o--', label='MC')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
ax.set(xlabel='Traffic Intensities', ylabel='Average RTT')
ax.grid()
plt.legend(loc='upper left')
plt.show()
fig.savefig("average_rtt.png")

fig, ax = plt.subplots()
plt.errorbar(x=labels, y=sp.packet_loss_mean, yerr=sp.packet_loss_std, fmt='o--', label='SP')
plt.errorbar(x=labels, y=mlu.packet_loss_mean, yerr=mlu.packet_loss_std, fmt='o--', label='MLU')
plt.errorbar(x=labels, y=mc.packet_loss_mean, yerr=mc.packet_loss_std, fmt='o--', label='MC')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
ax.set(xlabel='Traffic Intensities', ylabel='Packet Dropped Rate')
ax.grid()
plt.legend(loc='upper left')
plt.show()
fig.savefig("packet_dropped_rate.png")


# # df['latency_mean'].fillna(999.99, inplace=True)
# # df['latency_std'].fillna(0, inplace=True)
#
# print(df)
#
# labels = ['3.5', '4', '4.5', '5', '5.5', '6']
# x = np.arange(len(labels))
# width = 0.35
#
# fig, ax = plt.subplots()
# rects1 = ax.bar(x - width/2, df.packet_loss[6:12], width, label='V1')
# rects2 = ax.bar(x + width/2, df.packet_loss[12:], width, label='V2')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
# ax.set(xlabel='Traffic Intensities', ylabel='Packet Dropped Rate')
# ax.grid()
# plt.legend(loc='upper left')
# fig.savefig("packet_dropped_rate.png")
# plt.show()
#
# fig, ax = plt.subplots()
# rects1 = ax.bar(x - width/2, df.latency_mean[6:12], width, label='V1')
# rects1 = ax.bar(x + width/2, df.latency_mean[12:], width, label='V2')
# ax.set_xticks(x)
# ax.set_xticklabels(labels)
# ax.set(xlabel='Traffic Intensities', ylabel='Average RTT')
# ax.grid()
# plt.legend(loc='upper left')
# fig.savefig("average_rtt.png")
# plt.show()
