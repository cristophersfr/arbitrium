#!/usr/bin/env python

class HostRole(object):
    def __init__(self, host):
        self.proc_str = None
        self.proc = None
        self.host = host
        self.stdout = "/tmp/{0}-{1}.log".format(host.IP(),
                                                self.__class__.__name__)
        self.stderr = "/tmp/{0}-{1}.err".format(host.IP(),
                                                self.__class__.__name__)

    def init(self):
        pass

    def start(self):
        self.proc = self.host.popen(self.proc_str,
                                    sdout=open(self.stdout, "wb"),
                                    stderr=open(self.stderr, "wb"))

    def stop(self):
        try:
            self.proc.terminate()
        except Exception:
            pass

    def IP(self):
        return self.host.IP()

    def __repr__(self):
        return self.__str__()


class EmptyRole(HostRole):
    def __init__(self, host, name="Empty"):
        super(EmptyRole, self).__init__(host)
        self.name = name

    def init(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def __str__(self):
        return self.name
