#!/usr/bin/env python

import os
import random
import scipy.io
import pandas as pd
import numpy as np

from datacenter.roles import EmptyRole
from mininet.node import Node

from select import poll, POLLIN
from time import time


class AppProfile(object):
    def __init__(self, num_nodes):
        self.num_nodes = num_nodes
        self.vlan = -1
        self.apps = []
        self.nodes = []

    def assign_vlan(self, vlan):
        self.vlan = vlan

    def start(self):
        map(lambda p: p.init(), self.apps)
        map(lambda p: p.start(), self.apps)

    def stop(self):
        map(lambda p: p.stop(), self.apps)

    def create(self, hosts):
        pass

    def __repr__(self):
        return self.__str__()

    def check_hostlen(self, hostlen):
        if(hostlen < self.num_nodes):
            print("*** Warning: {0} expects {1} nodes, {2} given".format(
                self.__class__.__name__, self.num_nodes, hostlen))


class EmptyProfile(AppProfile):
    def __init__(self, num_nodes):
        super(EmptyProfile, self).__init__(num_nodes)

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        self.apps.extend([EmptyRole(h) for h in hosts])

    def __str__(self):
        return "Empty Profile"


def get_host(nodes, name):
    for node in nodes:
        if name == node.name:
            return node


class AbileneProfile(AppProfile):
    def __init__(self, num_nodes, duration, n_of_iterations=None):
        super(AbileneProfile, self).__init__(num_nodes)
        self.duration = duration
        self.n_of_iterations = n_of_iterations

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "Abilene Traffic"))

    def traffic(self, h1, h2, port, traffic_list, duration):
        srv_file = "./logs/server_{0}.log".format(port)
        srv_output = open(srv_file, 'a')
        clnt_file = "./logs/client_{0}.log".format(port)
        clnt_output = open(clnt_file, 'a')
        h1srv = "python3 server.py -sip {0} -sport {1}".format(h1.IP(), port)
        h2clnt = "python3 client.py -sip {0} -dip {1} -dport {2} -t {3} -l {4}".format(
            h2.IP(), h1.IP(), port, duration, traffic_list)

        print("Traffic: {0} -> {1} - {2} s".format(h1, h2, duration))
        srv_proc = h1.popen(h1srv, stdout=srv_output, stderr=srv_output)
        print('server {0} started job {1}'.format(h1, srv_proc.pid))
        clnt_proc = h2.popen(h2clnt, stdout=clnt_output, stderr=clnt_output)
        print('client {0} started job {1}'.format(h2, clnt_proc.pid))

        return {
            'server': h1,
            'client': h2,
            'srv_proc': srv_proc,
            'clnt_proc': clnt_proc,
        }

    def start(self):
        port_start = 12000
        # read the traffic matrix
        dt = scipy.io.loadmat('/home/cristopherpk@laccan.net/arbitrium/datacenter/datasets/SAND_TM_Estimation_Data.mat')
        # scale this traffic matrix to our scenario
        # 10 Mbps = 1.25 MB/s ideally = 1 250 000 Bytes / s
        scaling_factor = 1250000 / dt['X'].max()
        dt['X'] = dt['X'] * scaling_factor
        hosts = {
            'NYCM': 'h0',
            'CHIN': 'h1',
            'WASH': 'h2',
            'STTL': 'h3',
            'SNVA': 'h4',
            'LOSA': 'h5',
            'DNVR': 'h6',
            'KSCY': 'h7',
            'HSTN': 'h8',
            'ATLA': 'h9',
            'IPLS': 'h10',
        }
        # slice the dataset if there is a number of iterations
        if not self.n_of_iterations:
            self.n_of_iterations = dt['X'].shape[0]

        runners = []
        for i in range(len(dt['odnames'])):
            src, dst = dt['odnames'][i][0][0].split('-')
            srv = get_host(self.nodes, hosts[src])
            clnt = get_host(self.nodes, hosts[dst])
            amounts = dt['X'][:self.n_of_iterations, i].tolist()
            traffic_list = ' '.join(str(a) for a in amounts)
            runners.append(
                self.traffic(
                    srv,
                    clnt,
                    port_start,
                    traffic_list,
                    self.duration
                )
            )
            port_start += 1

        # Waiting the client processes to finish
        for r in runners:
            r['clnt_proc'].wait()
            print('client {0} finished job {1}'.format(r['client'], r['clnt_proc'].pid))
            r['srv_proc'].terminate()
            r['srv_proc'].wait()
            print('server {0} finished job {1}'.format(r['server'], r['srv_proc'].pid))

    def __str__(self):
        return "Abilene Real Traffic Profile"


class LogNormalProfile(AppProfile):
    def __init__(self, num_nodes, duration, n_of_iterations=None):
        super(LogNormalProfile, self).__init__(num_nodes)
        self.duration = duration
        self.n_of_iterations = n_of_iterations

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "LogNormal Traffic"))

    def traffic(self, h1, h2, port, traffic_list, duration):
        srv_file = "./logs/server_{0}.log".format(port)
        srv_output = open(srv_file, 'a')
        clnt_file = "./logs/client_{0}.log".format(port)
        clnt_output = open(clnt_file, 'a')
        h1srv = "python3 server.py -sip {0} -sport {1}".format(h1.IP(), port)
        h2clnt = "python3 client.py -sip {0} -dip {1} -dport {2} -t {3} -l {4}".format(
            h2.IP(), h1.IP(), port, duration, traffic_list)

        srv_proc = h1.popen(h1srv, stdout=srv_output, stderr=srv_output)
        print('server {0} started job {1}'.format(h1, srv_proc.pid))
        clnt_proc = h2.popen(h2clnt, stdout=clnt_output, stderr=clnt_output)
        print('client {0} started job {1}'.format(h2, clnt_proc.pid))

        return {'server': h1, 'client': h2, 'srv_proc': srv_proc, 'clnt_proc': clnt_proc}

    def start(self):
        port_start = 12000
        n_of_nodes = len(self.nodes)
        # read the traffic matrix
        random.seed(10)
        traffic = np.array([random.lognormvariate(12, 2) for i in range(self.n_of_iterations*(n_of_nodes**2))])

        # scale this traffic matrix to our scenario
        # 10 Mbps = 1.25 MB/s ideally = 1 250 000 Bytes / s
        # scaling_factor = 1250000 / traffic.max()
        # traffic = traffic.reshape(n_of_nodes, n_of_nodes, self.n_of_iterations) * scaling_factor
        traffic = traffic.reshape(n_of_nodes, n_of_nodes, self.n_of_iterations)
        # slice the dataset if there is a number of iterations
        if not self.n_of_iterations:
            raise ValueError("Missing variable")

        runners = []
        for i in range(n_of_nodes):
            for j in range(n_of_nodes):
                if i == j:
                    continue
                amounts = traffic[i, j, :self.n_of_iterations].tolist()
                traffic_list = ' '.join(str(a) for a in amounts)
                runners.append(
                    self.traffic(
                        self.nodes[i],
                        self.nodes[j],
                        port_start,
                        traffic_list,
                        self.duration
                    )
                )
                port_start += 1

        # Waiting the client processes to finish
        for r in runners:
            r['clnt_proc'].wait()
            print('client {0} finished job {1}'.format(r['client'], r['clnt_proc'].pid))
            r['srv_proc'].terminate()
            r['srv_proc'].wait()
            print('server {0} finished job {1}'.format(r['server'], r['srv_proc'].pid))

    def __str__(self):
        return "Log-normal Profile"


class AbileneLogNormalProfile(AppProfile):
    def __init__(self, num_nodes, duration, n_of_iterations=None):
        super(AbileneLogNormalProfile, self).__init__(num_nodes)
        self.duration = duration
        self.n_of_iterations = n_of_iterations

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "LogNormal Traffic"))

    def traffic(self, h1, h2, port, traffic_list, duration):
        srv_file = "./logs/server_{0}.log".format(port)
        srv_output = open(srv_file, 'a')
        clnt_file = "./logs/client_{0}.log".format(port)
        clnt_output = open(clnt_file, 'a')
        h1srv = "python3 server.py -sip {0} -sport {1}".format(h1.IP(), port)
        h2clnt = "python3 client.py -sip {0} -dip {1} -dport {2} -t {3} -l {4}".format(
            h2.IP(), h1.IP(), port, duration, traffic_list)

        srv_proc = h1.popen(h1srv, stdout=srv_output, stderr=srv_output)
        print('server {0} started job {1}'.format(h1, srv_proc.pid))
        clnt_proc = h2.popen(h2clnt, stdout=clnt_output, stderr=clnt_output)
        print('client {0} started job {1}'.format(h2, clnt_proc.pid))

        return {'server': h1, 'client': h2, 'srv_proc': srv_proc, 'clnt_proc': clnt_proc}

    def start(self):
        port_start = 12000
        # read the traffic matrix
        dt = scipy.io.loadmat('/home/cristopherpk@laccan.net/arbitrium/datacenter/datasets/SAND_TM_Estimation_Data.mat')
        traffic = pd.read_csv('/home/cristopherpk@laccan.net/arbitrium/datacenter/datasets/lognormal.csv')
        traffic = traffic.__array__()
        # scale this traffic matrix to our scenario
        # 10 Mbps = 1.25 MB/s ideally = 1 250 000 Bytes / s
        scaling_factor = 1250000 / traffic.max()
        traffic = traffic * scaling_factor
        hosts = {
            'NYCM': 'h0',
            'CHIN': 'h1',
            'WASH': 'h2',
            'STTL': 'h3',
            'SNVA': 'h4',
            'LOSA': 'h5',
            'DNVR': 'h6',
            'KSCY': 'h7',
            'HSTN': 'h8',
            'ATLA': 'h9',
            'IPLS': 'h10',
        }
        # slice the dataset if there is a number of iterations
        if not self.n_of_iterations:
            self.n_of_iterations = traffic.shape[0]

        runners = []
        for i in range(len(self.nodes)):
            src, dst = dt['odnames'][i][0][0].split('-')
            srv = get_host(self.nodes, hosts[src])
            clnt = get_host(self.nodes, hosts[dst])
            amounts = traffic[:self.n_of_iterations, i].tolist()
            traffic_list = ' '.join(str(a) for a in amounts)
            runners.append(
                self.traffic(
                    srv,
                    clnt,
                    port_start,
                    traffic_list,
                    self.duration
                )
            )
            port_start += 1

        # Waiting the client processes to finish
        for r in runners:
            r['clnt_proc'].wait()
            print('client {0} finished job {1}'.format(r['client'], r['clnt_proc'].pid))
            r['srv_proc'].terminate()
            r['srv_proc'].wait()
            print('server {0} finished job {1}'.format(r['server'], r['srv_proc'].pid))

    def __str__(self):
        return "Log-normal Abilene Profile"


class LogLogisticProfile(AppProfile):
    def __init__(self, num_nodes, duration, n_of_iterations=None):
        super(LogLogisticProfile, self).__init__(num_nodes)
        self.duration = duration
        self.n_of_iterations = n_of_iterations

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "Abilene Traffic"))

    def traffic(self, h1, h2, port, amount, duration):
        srv_file = "./logs/server_{0}.log".format(port)
        srv_output = open(srv_file, 'a')
        clnt_file = "./logs/client_{0}.log".format(port)
        clnt_output = open(clnt_file, 'a')
        h1srv = "python3 server.py -sip {0} -sport {1}".format(h1.IP(), port)
        h2clnt = "python3 client.py -sip {0} -dip {1} -dport {2} -a {3} -t {4}".format(
            h2.IP(), h1.IP(), port, amount, duration)

        print("Traffic: {0} -> {1} - {2} B/s - {3} s".format(h1, h2, amount, duration))
        srv_proc = h1.popen(h1srv, stdout=srv_output, stderr=srv_output)
        print('server {0} started job {1}'.format(h1, srv_proc.pid))
        clnt_proc = h2.popen(h2clnt, stdout=clnt_output, stderr=clnt_output)
        print('client {0} started job {1}'.format(h2, clnt_proc.pid))

        return {'server': h1, 'client': h2, 'srv_proc': srv_proc, 'clnt_proc': clnt_proc}

    def start(self):
        port_start = 12000
        # read the traffic matrix
        dt = scipy.io.loadmat('/home/cristopherpk@laccan.net/arbitrium/datacenter/datasets/SAND_TM_Estimation_Data.mat')
        traffic = pd.read_csv('/home/cristopherpk@laccan.net/arbitrium/datacenter/datasets/loglogistic.csv')
        traffic = traffic.__array__()
        # scale this traffic matrix to our scenario
        # 10 Mbps = 1.25 MB/s ideally = 1 250 000 Bytes / s
        scaling_factor = 1250000 / traffic.max()
        traffic = traffic * scaling_factor
        hosts = {
            'NYCM': 'h0',
            'CHIN': 'h1',
            'WASH': 'h2',
            'STTL': 'h3',
            'SNVA': 'h4',
            'LOSA': 'h5',
            'DNVR': 'h6',
            'KSCY': 'h7',
            'HSTN': 'h8',
            'ATLA': 'h9',
            'IPLS': 'h10',
        }
        # slice the dataset if there is a number of iterations
        if not self.n_of_iterations:
            self.n_of_iterations = traffic.shape[0]

        runners = []
        for i in range(len(dt['odnames'])):
            src, dst = dt['odnames'][i][0][0].split('-')
            srv = get_host(self.nodes, hosts[src])
            clnt = get_host(self.nodes, hosts[dst])
            amounts = traffic[:self.n_of_iterations, i].tolist()
            traffic_list = ' '.join(str(a) for a in amounts)
            runners.append(
                self.traffic(
                    srv,
                    clnt,
                    port_start,
                    traffic_list,
                    self.duration
                )
            )
            port_start += 1

        # Waiting the client processes to finish
        for r in runners:
            r['clnt_proc'].wait()
            print('client {0} finished job {1}'.format(r['client'], r['clnt_proc'].pid))
            r['srv_proc'].terminate()
            r['srv_proc'].wait()
            print('server {0} finished job {1}'.format(r['server'], r['srv_proc'].pid))

    def __str__(self):
        return "Log-logistic Abilene Profile"


class IperfProfile(AppProfile):
    def __init__(self, num_nodes, duration, max_flows=12):
        super(IperfProfile, self).__init__(num_nodes)
        self.bw = 0
        self.duration = duration
        self.max_flows = max_flows

    def iperfcmd(self, h1, h2, port):
        h1srv = "iperf -s -p {0} > /dev/null &".format(port)
        h2clnt = "iperf -M 9000 -c {0} -p {1} -u -b {2}M -t {3} > /dev/null &".format(h1.IP(), port,
                                                                                      self.bw, self.duration)
        print("Iperf: {0} -> {1}".format(h1, h2))
        h1.cmd(h1srv)
        h2.cmd(h2clnt)

    def update(self, bw):
        self.bw = bw

    def start(self):
        # start iperf
        port_start = 12000
        count = 0
        for h1 in self.nodes:
            for h2 in self.nodes:
                if h1 != h2:
                    if count < self.max_flows:
                        self.iperfcmd(h1, h2, port_start + count)
                        count += 1

    def stop(self):
        pass

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "Iperf Client"))

    def __str__(self):
        return "Iperf ({0})".format(self.duration)


class PingProfile(AppProfile):
    def __init__(self, num_nodes, duration):
        super(PingProfile, self).__init__(num_nodes)
        self.duration = duration
        self.log = 0

    def pingcmd(self, host, targetips):
        "Tell host to repeatedly ping targets"

        targetips = ' '.join(targetips)

        # Simple ping loop
        cmd = ('while true; do '
               ' for ip in %s; do ' % targetips +
               '  echo -n %s "->" $ip ' % host.IP() +
               '   `ping -c1 -w 1 $ip | grep rtt` ;'
               '  sleep 1;'
               ' done; '
               'done &')

        print('*** Host %s (%s) will be pinging ips: %s' %
              (host.name, host.IP(), targetips))

        host.cmd(cmd)

    def start(self):
        # create polling object
        fds = [host.stdout.fileno() for host in self.nodes]
        poller = poll()
        for fd in fds:
            poller.register(fd, POLLIN)

        # start pings
        ips = [host.IP() for host in self.nodes]

        for host in self.nodes:
            self.pingcmd(host, ips)

        # monitor output
        end_time = time() + self.duration
        while time() < end_time:
            readable = poller.poll(1000)
            for fd, _mask in readable:
                node = Node.outToNode[fd]
                # TODO: Store this output into InfluxDB.
                self.log.write(node.monitor().strip() + '\n')

        # stop pings
        for host in self.nodes:
            host.cmd('kill %while')

        self.log.close()

    def update(self, outfile):
        self.log = open(os.getcwd() + "/datacenter/logs/" + outfile, "a+")

    def stop(self):
        pass

    def create(self, hosts):
        self.check_hostlen(len(hosts))
        self.nodes.extend(hosts)
        for h in hosts:
            self.apps.append(EmptyRole(h, "Ping Client"))

    def __str__(self):
        return "Ping ({0})".format(self.duration)
