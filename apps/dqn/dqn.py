import os
import sys
import time
import signal
import logging
import threading
import math
import random
import json
import logging
import pandas as pd
import numpy as np
import networkx as nx
from collections import namedtuple
from itertools import count

# '/home/user/example/parent/child'
current_path = os.path.abspath('.')
print(current_path)
sys.path.append(current_path)

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from config import ONOS_IP, ONOS_PORT, VERBOSE
from config import POLLING_INTERVAL, TM_TRAINING_SET_SIZE, VERBOSE
from management.imr_manager import IMRManager
from management.topo_manager import TopoManager
from management.stats_manager import StatsManager

from utils import json_post_req, bps_to_human_string
from pprint import pprint


# Hyperparameters.
BATCH_SIZE = 64
GAMMA = 0.999
EPS_START = 0.9
EPS_END = 0.05
EPS_DECAY = 200
LR = 5e-4
TARGET_UPDATE = 4

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

# if gpu is to be used
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

torch.autograd.set_detect_anomaly(True)

torch.manual_seed(7)


class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """Saves a transition."""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class QNetwork(nn.Module):
    def __init__(self, num_states, num_actions, hidden_size=64):
        super(QNetwork, self).__init__()
        self.linear1 = nn.Linear(num_states, hidden_size)
        self.linear2 = nn.Linear(hidden_size, hidden_size)
        self.linear3 = nn.Linear(hidden_size, num_actions)

    def forward(self, x):
        x = F.relu(self.linear1(x))
        x = F.relu(self.linear2(x))
        x = torch.sigmoid(self.linear3(x))
        return x


class Agent(object):
    def __init__(self):
        self.verbose = VERBOSE
        self.paths = {}
        self.n_of_paths = 0
        self.alpha = 0.999
        self.steps_done = 0
        self.total_reward = 0
        self.memory = ReplayMemory(10000)
        self.topo = topo_manager.G.copy()
        self.intents = self.get_intents()
        n_of_flows = len(self.intents)
        self.state = torch.zeros(n_of_flows).to(device)

        # create the simple paths array
        for intent in self.intents:
            self.paths[intent] = {
                'paths': [],
                'starting_point': self.n_of_paths
            }
            for path in nx.all_simple_paths(self.topo, intent[:22], intent[22:]):
                self.paths[intent]['paths'].append(path)
                self.n_of_paths += 1

        print("Size of action space {0}".format(self.n_of_paths))


        self.policy_net = QNetwork(n_of_flows, self.n_of_paths).to(device)
        self.target_net = QNetwork(n_of_flows, self.n_of_paths).to(device)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()

        self.optimizer = optim.Adam(self.policy_net.parameters(), lr=LR)

    def get_intents(self):
        imr_manager.retrieve_monitored_intents_from_ONOS()
        monitored_intents = imr_manager.get_monitored_intents()
        intents = [intent[0] for intent in monitored_intents]
        return intents

    def get_worst_tm(self):
        # Get the latest version of topology and monitored events
        imr_manager.retrieve_monitored_intents_from_ONOS()
        topo_manager.retrieve_topo_from_ONOS()

        # Build the worst case Traffic Matrix by keeping only demands whose intentKeys are still monitored and
        # considering their worst-case historical value of the latest training interval
        tm_list = stats_manager.get_tm_store()[-TM_TRAINING_SET_SIZE:]
        monitored_intents = imr_manager.get_monitored_intents()

        worst_tm = {intentKey: max([tm[intentKey] if intentKey in tm else 0 for tm in tm_list])
                    for intentKey in monitored_intents}

        if VERBOSE:
            print("worst_tm")
            pprint({flow_id: bps_to_human_string(worst_tm[flow_id]) for flow_id in worst_tm})

        return worst_tm

    def from_tm_to_state(self, tm):
        return torch.tensor([amount for flow_id, amount in tm.items()], device=device)

    def utility(self, state):
        return state.apply_(lambda x: (x ** (1 - self.alpha)) / (1 - self.alpha)).sum()

    def get_reward(self, state, next_state):
        u0 = self.utility(state)
        u1 = self.utility(next_state)
        return u1 - u0

    def act(self, state):
        sample = random.random()
        eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * self.steps_done / EPS_DECAY)
        self.steps_done += 1
        if sample > eps_threshold:
            with torch.no_grad():
                action_values = self.policy_net(state.float())
                return action_values
        else:
            random_action = torch.zeros(self.n_of_paths)
            for intent in self.intents:
                path_index = random.randint(0, len(self.paths[intent]) - 1)
                random_action[(self.paths[intent]['starting_point']) + path_index] = 1.0
            return random_action

    def learn(self):
        if len(self.memory) < BATCH_SIZE:
            return

        transitions = self.memory.sample(BATCH_SIZE)
        batch = Transition(*zip(*transitions))

        states = torch.from_numpy(np.vstack([state for state in batch.state])).float().to(device)
        actions = torch.from_numpy(np.vstack([action for action in batch.action])).long().to(device)
        rewards = torch.from_numpy(np.vstack([reward for reward in batch.reward])).float().to(device)
        next_states = torch.from_numpy(np.vstack([next_state for next_state in batch.next_state])).float().to(
            device)

        state_action_values = self.policy_net(states)

        next_state_values = self.target_net(next_states).detach()

        # Compute the expected Q values
        expected_state_action_values = (next_state_values * GAMMA) + rewards

        # Compute Huber loss
        loss = F.smooth_l1_loss(state_action_values, expected_state_action_values)

        # Optimize the model
        self.optimizer.zero_grad()
        # Backward propagation
        loss.backward()
        print(loss.item())
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-1, 1)
        self.optimizer.step()

    def reroute(self, tm, action):

        reroute_msg = {'routingList': []}

        for flow_id, amount in sorted(tm.items(), key=lambda x: x[1], reverse=True):
            if amount == 0:
                continue

            intent_key, app_id, app_name = flow_id
            in_elem, out_elem = imr_manager.intentKey_to_inOutElements[flow_id]
            if self.verbose:
                print('Trying to route {0} for demand {1} -> {2}'.format(bps_to_human_string(amount), in_elem,
                                                                         out_elem))
            try:
                starting_i = self.paths[intent_key]['starting_point']
                ending_i = len(self.paths[intent_key]['paths']) - 1
                if ending_i > 0:
                    best_path = action[starting_i:(starting_i + ending_i)].max(0)[1]
                else:
                    best_path = action[starting_i].max(0)[1]
                path = self.paths[intent_key]['paths'][best_path]

                if self.verbose:
                    print('Found path {0}'.format(path))

                reroute_msg['routingList'].append(
                    {
                        'key': intent_key,
                        'appId': {
                            'id': app_id,
                            'name': app_name
                        },
                        'paths': [{
                            'path': path,
                            'weight': 1.0
                        }]
                    }
                )

            except nx.NetworkXNoPath:
                if self.verbose:
                    print('No path found')

        json_post_req(('http://{0}:{1}/onos/v1/imr/imr/reRouteIntents'.format(ONOS_IP, ONOS_PORT)),
                      json.dumps(reroute_msg))

    def step(self):
        if self.verbose:
            print('Running DQN...')

        action = self.act(self.state)
        # TODO: Remove this tm from reroute and use only the identifiers.
        self.reroute(self.get_worst_tm(), action)
        reroute_event.wait()

        # Get the next tm and the reward.
        next_state = self.from_tm_to_state(self.get_worst_tm())

        reward = self.get_reward(self.state, next_state)
        self.total_reward += reward.tolist()
        # Print out the trajectory
        print("step: {0} - reward: {1}".format(self.steps_done, self.total_reward))

        # Store the transition in memory
        self.memory.push(self.state, action, next_state, reward)

        # Perform one step of the optimization (on the target network)
        self.learn()

        self.state = next_state

        # Update the target network, copying all weights and biases in DQN
        if self.steps_done % TARGET_UPDATE == 0:
            self.target_net.load_state_dict(self.policy_net.state_dict())


class PollingThread(threading.Thread):
    def __init__(self):
        self._stopevent = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        while not self._stopevent.is_set():
            # Re-route intents as soon as TM_TRAINING_SET_SIZE TMs have been collected
            if len(stats_manager.get_tm_store()) > 0 and len(stats_manager.get_tm_store()) % TM_TRAINING_SET_SIZE == 0:
                reroute_event.set()

            threading.Thread(target=stats_manager.poll_stats).start()
            time.sleep(POLLING_INTERVAL)

    def stop(self):
        self._stopevent.set()


class DQNThread(threading.Thread):
    def __init__(self):
        self._stopevent = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        dqn_agent = Agent()
        while not self._stopevent.is_set():
            reroute_event.wait()
            if self._stopevent.is_set():
                return

            logging.info("Re-routing Intents...")
            reroute_event.clear()

            dqn_agent.step()

    def stop(self):
        self._stopevent.set()
        reroute_event.set()


def handler_stop_signals(signum, frame):
    polling_thread.stop()
    dqn_thread.stop()
    logging.info("Killing all the threads...")
    sys.exit(0)


if __name__ == '__main__':
    topo_manager = TopoManager()
    topo_manager.draw_topo()
    stats_manager = StatsManager()
    imr_manager = IMRManager()

    reroute_event = threading.Event()

    polling_thread = PollingThread()
    dqn_thread = DQNThread()
    polling_thread.start()
    dqn_thread.start()

    signal.signal(signal.SIGINT, handler_stop_signals)

    logging.info('Press any key to exit')
    input('')
    logging.info('Killing all the threads...')
    polling_thread.stop()
    dqn_thread.stop()