import os
import pprint
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

results = []

file = open("dqn.log", "r")

for line in file:
    data = line.split(" ")
    results.append(float(data[4]))

df = pd.DataFrame(data=results)

df.plot()

plt.show()