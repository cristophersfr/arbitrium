import os
import sys
import time
import signal
import logging
import threading

from pprint import pprint

# '/home/user/example/parent/child'
current_path = os.path.abspath('.')
print(current_path)
sys.path.append(current_path)

from config import POLLING_INTERVAL, TM_TRAINING_SET_SIZE, VERBOSE
from management.imr_manager import IMRManager
from management.topo_manager import TopoManager
from management.stats_manager import StatsManager
from utils import bps_to_human_string

from apps.mc.myopic import MyopicAlgorithm


class PollingThread(threading.Thread):
    def __init__(self):
        self._stopevent = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        while not self._stopevent.is_set():
            # Re-route intents as soon as TM_TRAINING_SET_SIZE TMs have been collected
            if len(statsManager.get_tm_store()) > 0 and len(statsManager.get_tm_store()) % TM_TRAINING_SET_SIZE == 0:
                reroute_event.set()

            threading.Thread(target=statsManager.poll_stats).start()
            time.sleep(POLLING_INTERVAL)

    def stop(self):
        self._stopevent.set()


class ReroutingThread(threading.Thread):
    def __init__(self):
        self._stopevent = threading.Event()
        threading.Thread.__init__(self)

    def run(self):
        while not self._stopevent.is_set():
            reroute_event.wait()
            if self._stopevent.is_set():
                return

            logging.info("Re-routing Intents...")
            reroute_event.clear()

            # Get the latest version of topology and monitored events
            imrManager.retrieve_monitored_intents_from_ONOS()
            topoManager.retrieve_topo_from_ONOS()

            # Build the worst case Traffic Matrix by keeping only demands whose intentKeys are still monitored and
            # considering their worst-case historical value of the latest training interval
            tm_list = statsManager.get_tm_store()[-TM_TRAINING_SET_SIZE:]
            monitored_intents = imrManager.get_monitored_intents()

            worst_tm = {intentKey: max([tm[intentKey] if intentKey in tm else 0 for tm in tm_list])
                        for intentKey in monitored_intents}

            if VERBOSE:
                print("worst_tm")
                pprint({flow_id: bps_to_human_string(worst_tm[flow_id]) for flow_id in worst_tm})

            myopicManager.run(worst_tm, topoManager, imrManager)

    def stop(self):
        self._stopevent.set()
        reroute_event.set()


def handler_stop_signals(signum, frame):
    pollingThread.stop()
    reroutingThread.stop()
    logging.info("Killing all the threads...")
    sys.exit(0)


if __name__ == "__main__":
    topoManager = TopoManager()
    topoManager.draw_topo()
    statsManager = StatsManager()
    imrManager = IMRManager()
    myopicManager = MyopicAlgorithm()

    reroute_event = threading.Event()

    pollingThread = PollingThread()
    reroutingThread = ReroutingThread()
    pollingThread.start()
    reroutingThread.start()

    signal.signal(signal.SIGINT, handler_stop_signals)

    logging.info('Press any key to exit')
    input('')
    logging.info('Killing all the threads...')
    pollingThread.stop()
    reroutingThread.stop()
