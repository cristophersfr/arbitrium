import json
import logging
import random
import networkx as nx

from config import ONOS_IP, ONOS_PORT, VERBOSE
from utils import json_post_req, bps_to_human_string
from pprint import pprint


class MyopicAlgorithm(object):
    def __init__(self):
        self.verbose = VERBOSE
        self.alpha = 1
        self.Y = []

    def initial_state_matrix(self, edges):
        Y_t = dict()
        for edge in edges:
            Y_t[edge] = 0

        return Y_t

    def get_the_best_path(self, R_j, Z_t, s_j):
        w = 0
        best_path = []
        for path in map(nx.utils.pairwise, R_j):
            w_i = 0
            path = list(path)
            for edge in path:
                try:
                    Z_l = Z_t[edge]
                except KeyError:
                    Z_l = Z_t[tuple(reversed(edge))]

                delta_ij = self.g(Z_l + s_j) - self.g(Z_l)
                w_i = w_i + delta_ij
            # If we do not know any path yet
            if (len(best_path) == 0) or (w_i < w):
                best_path = path
                w = w_i
            elif w_i == w: # If we know the best path, but its cost is higher
                if random.uniform(0, 1) > 0.5:
                    best_path = path
                    w = w_i


        chosen_path = []
        for e in best_path:
            if e[0] not in chosen_path:
                chosen_path.append(e[0])
            if e[1] not in chosen_path:
                chosen_path.append(e[1])

        return chosen_path

    def g(self, x):
        return (x ** (1 + self.alpha)) / (1 + self.alpha)

    def Z_t(self, Y_t):
        Z = {edge: Y_t[edge] for edge in Y_t}
        return Z

    def add_flow(self, Y_t, path_i, sizeof_j):
        for i in range(0,len(path_i)-1):
            try:
                edge = (path_i[i], path_i[i + 1])
                Y_t[edge] = Y_t[edge] + sizeof_j
            except KeyError:
                edge = (path_i[i + 1], path_i[i])
                Y_t[edge] = Y_t[edge] + sizeof_j

    def run(self, tm, topoManager, imrManager):
        if self.verbose:
            print('Running Myopic Algorithm...')

        # get the network topology
        topo = topoManager.G.copy()

        # initialize the network state matrix
        Y_t = self.initial_state_matrix(topo.edges())

        # rerouting solution
        reroute_msg = {'routingList': []}

        # iterate over flows (sorted by amount, in decreasing order)
        for flow_id, s_j in sorted(tm.items(), key=lambda x: x[1], reverse=True):
            # If there is no traffic, do nothing.
            if s_j == 0:
                continue

            intent_key, app_id, app_name = flow_id
            in_elem, out_elem = imrManager.intentKey_to_inOutElements[flow_id]
            if self.verbose:
                print('Trying to route {0} for demand {1} -> {2}'.format(bps_to_human_string(s_j), in_elem,
                                                                         out_elem))
            try:
                a_j = list(topo[in_elem])[0]
                d_j = list(topo[out_elem])[0]
                R_j = nx.all_simple_paths(topo, a_j, d_j)
                Z = self.Z_t(Y_t)
                path = self.get_the_best_path(R_j, Z, s_j)
                self.add_flow(Y_t, path, s_j)

                if self.verbose:
                    print('Found path {0}'.format(path))

                reroute_msg['routingList'].append(
                    {
                        'key': intent_key,
                        'appId': {
                            'id': app_id,
                            'name': app_name
                        },
                        'paths': [{
                            'path': path,
                            'weight': 1.0
                        }]
                    }
                )

            except nx.NetworkXNoPath:
                if self.verbose:
                    print('No path found')

        self.Y = Y_t

        if self.verbose:
            logging.info('reroute_msg config:')
            # pprint(reroute_msg)
            # print(self.Y)

        # json_post_req(('http://{0}:{1}/onos/v1/imr/imr/reRouteIntents'.format(ONOS_IP, ONOS_PORT)),
        #               json.dumps(reroute_msg))



