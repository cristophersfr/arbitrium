# Copyright 2014 Dr. Greg M. Bernstein
""" A script to set up a Multi-layer capacity allocation formulation problem example.
"""
from networkx.readwrite import json_graph
from LinkPathCapAlloc import multi_link_path_alloc, multi_link_path_allocSPA
from Utilities.utilities import gen_cand_paths
import json
from helper import extract_layer, sol_paths_j


if __name__ == '__main__':
    g = json_graph.node_link_graph(json.load(open('Example1d.json')))
    demands = {('E1', 'E4'): 23, ('E2', 'E3'): 18, ('E2', 'E6'): 19, ('E3', 'E4'): 17}
    g_lower = extract_layer(g, 'W')
    g_upper = extract_layer(g, 'E')
    layers = ['W', 'E']
    upaths = gen_cand_paths(g_upper, demands, 4)
    # Create a map to take us from upper layer nodes to corresponding lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create lower layer "demands" based on upper layer links
    # Will feed this to the candidate paths utility
    ldemands = {}
    for e in g_upper.edges():
        ldemands[ul_map[e[0]], ul_map[e[1]]] = 1.0
    lpaths = gen_cand_paths(g_lower, ldemands, 5)
    prob, udemand_paths, ldemand_paths, ulink_cap = multi_link_path_alloc(g,demands, layers, upaths, lpaths)
    #print prob
    prob.solve()
    print "Allocation problem objective = {}".format(prob.objective.value())
    u_sol_paths = sol_paths_j(g, udemand_paths, upaths, demands)
    print json.dumps(u_sol_paths)
    for e in g_upper.edges():
        ldemands[ul_map[e[0]], ul_map[e[1]]] = ulink_cap[e].varValue
    l_sol_paths = sol_paths_j(g, ldemand_paths, lpaths, ldemands, ul_map=ul_map)
    print json.dumps(l_sol_paths)
    prob2, udemand_paths2, ldemand_paths2, ulink_cap2 = multi_link_path_allocSPA(g,demands, layers, upaths, lpaths)
    prob2.solve()
    print "Single Path Allocation problem objective = {}".format(prob2.objective.value())
    u_sol_paths2 = sol_paths_j(g, udemand_paths2, upaths, demands, binary=True)
    print json.dumps(u_sol_paths2)
    for e in g_upper.edges():
        ldemands[ul_map[e[0]], ul_map[e[1]]] = ulink_cap2[e].varValue
    l_sol_paths2 = sol_paths_j(g, ldemand_paths2, lpaths, ldemands, binary=True, ul_map=ul_map)
    print json.dumps(l_sol_paths2)

