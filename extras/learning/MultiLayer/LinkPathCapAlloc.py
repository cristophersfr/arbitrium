# Copyright 2014 Dr. Greg M. Bernstein
""" Multi-Layer Capacity Allocation in Link-Path formulation
"""
__author__ = 'Greg Bernstein'
from Utilities.utilities import link_in_path, pathCost
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum
from helper import extract_layer


def multi_link_path_alloc(g, demands, layers, upaths, lpaths):
    """ Formulate a two layer capacity allocation problem.
        This formulation returns a LP problem.

        Parameters
        ----------
        g : networkx.Graph
            A graph representing a two layer network. Every node has a "layer" attribute. Links
            have either a "layer" attribute or are "adaptation" links between layers.
        demands : dictionary
            Demand dictionary indexed by tuples of upper layer nodes.
        layers : list
            A list of strings that denote the layers from the bottom up. For example ['W', 'E'] might
            be used to indicate Ethernet over WDM.
        upaths : dictionary
            Dictionary of upper layer candidate paths for the demand pairs.
        lpaths : dictionary
            Dictionary of lower layer candidate paths for the upper layer links.

        Returns
        -------
        prob, udemand_paths, ldemand_paths, ulink_cap : LpProblem, dictionary, dictionary, dictionary
            where udemand_paths is a dictionary of the upper layers demand path variables, ldemand_paths is
            a dictionary of the lower layers demand path variables, and ulink_cap is a dictionary of the
            upper layer link capacity variables.
    """
    demand_list = sorted(demands.keys())
    prob = LpProblem("Multi-Layer Cap Allocation Link Path Formulation", LpMinimize)
    # Create upper and lower layer graphs to help in problem formulation
    g_upper = extract_layer(g, layers[1])
    g_lower = extract_layer(g, layers[0])
    ulink_list = sorted(g_upper.edges())
    llink_list = sorted(g_lower.edges())
    # Create a map between upper and lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create upper layer demand path variables
    udemand_paths = {}
    for d in demand_list:
        for p in range(len(upaths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            udemand_paths[d, p] = LpVariable(name, 0)
    # Create lower layer demand path variables
    ldemand_paths = {}
    for e in ulink_list:  # Upper layer links are lower layer demands
        for p in range(len(lpaths[ul_map[e[0]], ul_map[e[1]]])):
            name = "zE{}_{}Q_{}".format(e[0], e[1], str(p))
            ldemand_paths[e, p] = LpVariable(name, 0)
    # Create upper layer link capacity variables
    ulink_cap = {}
    for e in ulink_list:
        name = "yE{}_{}".format(e[0], e[1])
        ulink_cap[e] = LpVariable(name, 0)
    # Add the objective function which for now is the sum of all the lower layer
    # demand path variables times their respective path length.
    tmp_list = []
    for e in ulink_list:
        d = ul_map[e[0]], ul_map[e[1]]  # Lower layer demand
        for p in range(len(lpaths[d])):
            tmp_list.append(pathCost(lpaths[d][p], g_lower)*ldemand_paths[e, p])
    prob += lpSum(tmp_list)
    # Upper layer link capacity constraints
    for link in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[link])
        for d in demand_list:
            for p in range(len(upaths[d])):
                if link_in_path(link, upaths[d][p]):
                    tmp_list.append(udemand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    for link in llink_list:
        tmp_list = []
        for e in ulink_list:
            d = ul_map[e[0]], ul_map[e[1]]
            for p in range(len(lpaths[d])):
                if link_in_path(link, lpaths[d][p]):
                    tmp_list.append(ldemand_paths[e, p])
        link_name = "W{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= g[link[0]][link[1]]['capacity'], "LinkCap|" + link_name

    # upper layer demand satisfaction constraints
    for d in demand_list:
        tmp_list = []
        for p in range(len(upaths[d])):
            tmp_list.append(udemand_paths[d, p])
        constraint_name = "UDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name

    # lower layer demand satisfaction constraints
    for e in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[e])
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list.append(ldemand_paths[e, p])
        constraint_name = "LDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 0, constraint_name
    return prob, udemand_paths, ldemand_paths, ulink_cap


def multi_link_path_allocSPA(g, demands, layers, upaths, lpaths, bigC=1.0e5):
    """ Formulates a two layer single path capacity allocation problem.
        This formulation returns a MIP problem.

        Parameters
        ----------
        g : networkx.Graph
            A graph representing a two layer network. Every node has a "layer" attribute. Links
            have either a "layer" attribute or are "adaptation" links between layers.
        demands : dictionary
            Demand dictionary indexed by tuples of upper layer nodes.
        layers : list
            A list of strings that denote the layers from the bottom up. For example ['W', 'E'] might
            be used to indicate Ethernet over WDM.
        upaths : dictionary
            Dictionary of upper layer candidate paths for the demand pairs.
        lpaths : dictionary
            Dictionary of lower layer candidate paths for the upper layer links.

        Returns
        -------
        prob, udemand_paths, ldemand_paths, ulink_cap : LpProblem, dictionary, dictionary, dictionary
            where udemand_paths is a dictionary of the upper layers demand path variables, ldemand_paths is
            a dictionary of the lower layers demand path variables, and ulink_cap is a dictionary of the
            upper layer link capacity variables.


    """
    demand_list = sorted(demands.keys())
    prob = LpProblem("Multi-Layer Cap Allocation Link Path Formulation", LpMinimize)
    # Create upper and lower layer graphs to help in problem formulation
    g_upper = extract_layer(g, layers[1])
    g_lower = extract_layer(g, layers[0])
    ulink_list = sorted(g_upper.edges())
    llink_list = sorted(g_lower.edges())
    # Create a map between upper and lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create upper layer unit demand path variables
    udemand_paths = {}
    for d in demand_list:
        for p in range(len(upaths[d])):
            name = "uD{}_{}P_{}".format(d[0], d[1], str(p))
            udemand_paths[d, p] = LpVariable(name, 0, cat='Binary')
    # Create lower layer demand path variables
    ldemand_paths = {}
    for e in ulink_list:  # Upper layer links are lower layer demands
        for p in range(len(lpaths[ul_map[e[0]], ul_map[e[1]]])):
            name = "zE{}_{}Q_{}".format(e[0], e[1], str(p))
            ldemand_paths[e, p] = LpVariable(name, 0)
    # Create unit lower layer demand path variables
    uldemand_paths = {}
    for e in ulink_list:  # Upper layer links are lower layer demands
        for p in range(len(lpaths[ul_map[e[0]], ul_map[e[1]]])):
            name = "rE{}_{}Q_{}".format(e[0], e[1], str(p))
            uldemand_paths[e, p] = LpVariable(name, 0, cat='Binary')
    # Create upper layer link capacity variables
    ulink_cap = {}
    for e in ulink_list:
        name = "yE{}_{}".format(e[0], e[1])
        ulink_cap[e] = LpVariable(name, 0)

    # Add the objective function which for now is the sum of all the lower layer
    # demand path variables times their respective path length.
    tmp_list = []
    for e in ulink_list:
        d = ul_map[e[0]], ul_map[e[1]]  # Lower layer demand
        for p in range(len(lpaths[d])):
            tmp_list.append(pathCost(lpaths[d][p], g_lower)*ldemand_paths[e, p])
    prob += lpSum(tmp_list)
    # Upper layer link capacity constraints
    for link in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[link])
        for d in demand_list:
            for p in range(len(upaths[d])):
                if link_in_path(link, upaths[d][p]):
                    tmp_list.append(demands[d]*udemand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    for link in llink_list:
        tmp_list = []
        for e in ulink_list:
            d = ul_map[e[0]], ul_map[e[1]]
            for p in range(len(lpaths[d])):
                if link_in_path(link, lpaths[d][p]):
                    tmp_list.append(ldemand_paths[e, p])
        link_name = "W{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= g[link[0]][link[1]]['capacity'], "LinkCap|" + link_name

    # upper layer demand satisfaction constraints
    for d in demand_list:
        tmp_list = []
        for p in range(len(upaths[d])):
            tmp_list.append(udemand_paths[d, p])
        constraint_name = "UDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 1.0, constraint_name

    # lower layer demand satisfaction constraints
    for e in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[e])
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list.append(ldemand_paths[e, p])
        constraint_name = "LDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 0, constraint_name

    # unit lower layer demand satisfaction constraints
    for e in ulink_list:
        tmp_list = []
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list.append(uldemand_paths[e, p])
        constraint_name = "UnitLDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 1.0, constraint_name

    # unit lower layer path useage constraints
    for e in ulink_list:
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list = []
            tmp_list.append(ldemand_paths[e, p])
            tmp_list.append(-bigC*uldemand_paths[e,p])
            constraint_name = "UnitLUsage_{}_{}P_{}".format(d[0], d[1], str(p))
            prob += lpSum(tmp_list) <= 0, constraint_name

    return prob, udemand_paths, ldemand_paths, ulink_cap

