# Copyright 2014 Dr. Greg M. Bernstein
""" Helper functions for multi-layer networking examples
"""
from Utilities.utilities import pathCost

def convert_multi_layer(g, layers):
    """ Sets layer attributes on nodes and links based on a single letter prefix
        at the start of the node string.
        The graph `g` will be modified so that each node will have a "layer" attribute,
        and each link will have either a "layer" attribute or an "adaptation" attribute.

        Parameters
        ----------
        g : networkx.Graph or networkx.DiGraph
            A network graph to which we will append attributes used in rendering. This
            graph will be modified with the following additional node attributes: 'layer';
            Link attributes: 'layer', 'adaptation'

        layers : list of single character strings
            A list of prefixes such as ['W', 'E'] used in node names to denote layers such
            as Ethernet.

    """
    for n in g.nodes():
        for l in layers:
            if l in n:
                g.node[n]['layer'] = l

    for e in g.edges():
        if e[0][0] != e[1][0]:
            g[e[0]][e[1]]['adaptation'] = True
        else:
            for l in layers:
                if l in e[0]:
                    g[e[0]][e[1]]['layer'] = l


def extract_layer(g, layer_string):
    """ Extracts a particular layer network from the given network.

        Parameters
        ----------
        g : networkx.Graph
            The graph describing the original network (can be directed or undirected)
        layer_string : string
            a string indicating the layer network to be extracted.

        Returns
        -------
        layer_g : networkx.Graph
            a graph consisting of a single layer network.
    """
    g_layer = g.copy()
    for n in g.nodes():
        if g.node[n]['layer'] != layer_string:
            g_layer.remove_node(n)
    return g_layer




def sol_paths_j(g, d_paths, can_paths, demands, binary=False, ul_map=None, m_mod=None):
    """ Puts solution paths in a form suitable for JSON export.
        See MultiExample1 and DimExample1 for use at both upper and lower layers.

        Parameters
        ----------
        g : networkx.Graph
            g the original network
        d_paths : dictionary
            solution demand path variables indexed by ((nA,nZ), p) where nA, nZ are the A and Z nodes of the demand,
            and p is the path index (integer).
        can_paths : dictionary
            candidate paths used in problem solution
        demands : dictionary
            the problems demand dictionary
        binary : boolean
            indicates if the d_path variables are binary, i.e., as in an SPA problem.
        ul_map : dictionary
            a mapping between upper and lower layer nodes.
        m_mod : float
            modularity of links.

        Returns
        -------
        sol_paths : list
            Gives nice information about solution paths in JSON format:
            [{"load": 17.5, "nodeList": ["N2", "N1", "N0"], "cost": 25, "ratio": 0.4375}, ...]
    """
    info_paths = []
    for p in d_paths.keys():
        load = d_paths[p].varValue
        if not m_mod:
            m_mod = 1.0
        if ul_map:
            d = ul_map[p[0][0]], ul_map[p[0][1]]
        else:
            d = p[0]
        if load > 0.001 * demands[d]:
            if binary:
                info = {"nodeList": can_paths[d][p[1]], "load": m_mod*demands[d], "cost": pathCost(can_paths[d][p[1]],g),
                        "ratio": 1.0}
            else:
                info = {"nodeList": can_paths[d][p[1]], "load": m_mod*load, "cost": pathCost(can_paths[d][p[1]],g),
                        "ratio": load/demands[d]}
            info_paths.append(info)
    return info_paths