# Copyright 2014 Dr. Greg M. Bernstein
""" A script to set up Multi-layer dimensioning formulation problem example 2.
    Note that commented out code can be used to generate new examples.
"""
from networkx.readwrite import json_graph
import Utilities.jsonconverter as jc
from Utilities.utilities import gen_cand_paths
import json
import helper
import LinkPathDim as lpd

if __name__ == '__main__':
    g = json_graph.node_link_graph(json.load(open('DimExample1c.json')))
    # ut.wt_by_distance(g)
    # helper.convert_multi_layer(g,['W', 'E'])
    # json_graph.dump(g, open('DimExample1b.json', 'w'))
    g_upper = helper.extract_layer(g, "E")
    g_lower = helper.extract_layer(g, "W")
    # demands = ut.gen_rand_demands(g_upper.nodes(), 10, 5, 25)
    # json.dump(jc.demands_to_j(demands), open("DemandsDimEx1.json", 'w'))
    demands = jc.j_to_demands(json.load(open("DemandsDimEx1.json")))
    # print demands
    # # used to set the upper layer link weights to 1.
    # for e in g_upper.edges():
    #     g[e[0]][e[1]]['weight'] = 1.0
    # json_graph.dump(g, open('DimExample1c.json', 'w'))
    layers = ['W', 'E']
    m_mod = 10.0
    n_mod = 40.0
    upaths = gen_cand_paths(g_upper, demands, 5)
    # Create a map to take us from upper layer nodes to corresponding lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create lower layer "demands" based on upper layer links
    # Will feed this to the candidate paths utility
    ldemands = {}
    for e in g_upper.edges():
        ldemands[ul_map[e[0]], ul_map[e[1]]] = 1.0
    lpaths = gen_cand_paths(g_lower, ldemands, 7)
    prob, udemand_paths, ldemand_paths, ulink_cap, llink_cap = lpd.multi_link_path_mod_dim(g, demands, layers,
                                                                                            upaths, lpaths, m_mod, n_mod)
    prob.solve()
    print "Modular Dimensioning problem objective = {}".format(prob.objective.value())
    u_sol_paths = helper.sol_paths_j(g, udemand_paths, upaths, demands)
    print json.dumps(u_sol_paths)
    for e in g_upper.edges():
        ldemands[ul_map[e[0]], ul_map[e[1]]] = ulink_cap[e].varValue
    l_sol_paths = helper.sol_paths_j(g, ldemand_paths, lpaths, ldemands, ul_map=ul_map, m_mod=10.0)
    print json.dumps(l_sol_paths)

