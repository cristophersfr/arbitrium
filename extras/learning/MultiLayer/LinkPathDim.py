""" Multi-Layer Dimensioning in Link-Path formulation
"""
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum
from Utilities.utilities import link_in_path
from helper import extract_layer


def multi_link_path_dim(g, demands, layers, upaths, lpaths):
    """ Formulates a two layer link dimensioning problem.
        This formulation produces a LP problem.

        Parameters
        ----------
        g : networkx.Graph
            A graph representing a two layer network. Every node has a "layer" attribute. Links
            have either a "layer" attribute or are "adaptation" links between layers.
        demands : dictionary
            Demand dictionary indexed by tuples of upper layer nodes.
        layers : list
            A list of strings that denote the layers from the bottom up. For example ['W', 'E'] might
            be used to indicate Ethernet over WDM.
        upaths : dictionary
            Dictionary of upper layer candidate paths for the demand pairs.
        lpaths : dictionary
            Dictionary of lower layer candidate paths for the upper layer links.

        Returns
        -------
        prob, udemand_paths, ldemand_paths, ulink_cap, llink_cap : LpProblem, dictionaries
            where udemand_paths is a dictionary of the upper layers demand path variables, ldemand_paths is
            a dictionary of the lower layers demand path variables, and ulink_cap is a dictionary of the
            upper layer link capacity variables, and llink_cap is a dictionary of the lower layer link
            capacities, i.e., the final item we are solving for.
    """
    demand_list = sorted(demands.keys())
    prob = LpProblem("Multi-Layer Cap Allocation Link Path Formulation", LpMinimize)
    # Create upper and lower layer graphs to help in problem formulation
    g_upper = extract_layer(g, layers[1])
    g_lower = extract_layer(g, layers[0])
    ulink_list = sorted(g_upper.edges())
    llink_list = sorted(g_lower.edges())
    # Create a map between upper and lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create upper layer demand path variables
    udemand_paths = {}
    for d in demand_list:
        for p in range(len(upaths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            udemand_paths[d, p] = LpVariable(name, 0)
    # Create lower layer demand path variables
    ldemand_paths = {}
    for e in ulink_list:  # Upper layer links are lower layer demands
        for p in range(len(lpaths[ul_map[e[0]], ul_map[e[1]]])):
            name = "zE{}_{}Q_{}".format(e[0], e[1], str(p))
            ldemand_paths[e, p] = LpVariable(name, 0)
    # Create upper layer link capacity variables
    ulink_cap = {}
    for e in ulink_list:
        name = "yE{}_{}".format(e[0], e[1])
        ulink_cap[e] = LpVariable(name, 0)
    # Create lower layer link capacity variables
    llink_cap = {}
    for e in llink_list:
        name = "uG{}_{}".format(e[0], e[1])
        llink_cap[e] = LpVariable(name, 0)
    # Add the objective function which is the sum of the link capacities times their
    # costs.
    tmp_list = []
    for e in ulink_cap.keys():
        tmp_list.append(g[e[0]][e[1]]["weight"]*ulink_cap[e])
    for e in llink_cap.keys():
        tmp_list.append(g[e[0]][e[1]]["weight"]*llink_cap[e])
    prob += lpSum(tmp_list)

    # Upper layer link capacity constraints
    for link in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[link])
        for d in demand_list:
            for p in range(len(upaths[d])):
                if link_in_path(link, upaths[d][p]):
                    tmp_list.append(udemand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    for link in llink_list:
        tmp_list = []
        tmp_list.append(-1.0*llink_cap[link])
        for e in ulink_list:
            d = ul_map[e[0]], ul_map[e[1]]
            for p in range(len(lpaths[d])):
                if link_in_path(link, lpaths[d][p]):
                    tmp_list.append(ldemand_paths[e, p])
        link_name = "W{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    # upper layer demand satisfaction constraints
    for d in demand_list:
        tmp_list = []
        for p in range(len(upaths[d])):
            tmp_list.append(udemand_paths[d, p])
        constraint_name = "UDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name

    # lower layer demand satisfaction constraints
    for e in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[e])
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list.append(ldemand_paths[e, p])
        constraint_name = "LDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 0, constraint_name
    return prob, udemand_paths, ldemand_paths, ulink_cap, llink_cap




def multi_link_path_mod_dim(g, demands, layers, upaths, lpaths, m_mod, n_mod):
    """ Formulations a two layer modular dimensioning problem.
        Both upper and lower layer links are assumed to come in discrete
        units of capacity. This gives a MIP problem.

        Parameters
        ----------
        g : networkx.Graph
            A graph representing a two layer network. Every node has a "layer" attribute. Links
            have either a "layer" attribute or are "adaptation" links between layers.
        demands : dictionary
            Demand dictionary indexed by tuples of upper layer nodes.
        layers : list
            A list of strings that denote the layers from the bottom up. For example ['W', 'E'] might
            be used to indicate Ethernet over WDM.
        upaths : dictionary
            Dictionary of upper layer candidate paths for the demand pairs.
        lpaths : dictionary
            Dictionary of lower layer candidate paths for the upper layer links.
        m_mod, n_mod : float
            modular capacities of the upper and lower layer links respectively.

        Returns
        -------
        prob, udemand_paths, ldemand_paths, ulink_cap, llink_cap : LpProblem, dictionaries
            where udemand_paths is a dictionary of the upper layers demand path variables, ldemand_paths is
            a dictionary of the lower layers demand path variables, and ulink_cap is a dictionary of the
            upper layer link capacity variables, and llink_cap is a dictionary of the lower layer link
            capacities, i.e., the final item we are solving for.
    """
    demand_list = sorted(demands.keys())
    prob = LpProblem("Multi-Layer Cap Allocation Link Path Formulation", LpMinimize)
    # Create upper and lower layer graphs to help in problem formulation
    g_upper = extract_layer(g, layers[1])
    g_lower = extract_layer(g, layers[0])
    ulink_list = sorted(g_upper.edges())
    llink_list = sorted(g_lower.edges())
    # Create a map between upper and lower layer nodes
    ul_map = {}
    for nu in g_upper.nodes():
        for e in g.edges(nu):
            if 'adaptation' in g[e[0]][e[1]]:
                ul_map[nu] = e[1]
    # Create upper layer demand path variables
    udemand_paths = {}
    for d in demand_list:
        for p in range(len(upaths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            udemand_paths[d, p] = LpVariable(name, 0)
    # Create lower layer demand path variables
    ldemand_paths = {}
    for e in ulink_list:  # Upper layer links are lower layer demands
        for p in range(len(lpaths[ul_map[e[0]], ul_map[e[1]]])):
            name = "zE{}_{}Q_{}".format(e[0], e[1], str(p))
            ldemand_paths[e, p] = LpVariable(name, 0, cat='Integer')
    # Create upper layer link capacity variables
    ulink_cap = {}
    for e in ulink_list:
        name = "yE{}_{}".format(e[0], e[1])
        ulink_cap[e] = LpVariable(name, 0, cat='Integer')
    # Create lower layer link capacity variables
    llink_cap = {}
    for e in llink_list:
        name = "uG{}_{}".format(e[0], e[1])
        llink_cap[e] = LpVariable(name, 0, cat='Integer')
    # Add the objective function which is the sum of the link capacities times their
    # costs.
    tmp_list = []
    for e in ulink_cap.keys():
        tmp_list.append(m_mod*g[e[0]][e[1]]["weight"]*ulink_cap[e])
    for e in llink_cap.keys():
        tmp_list.append(n_mod*g[e[0]][e[1]]["weight"]*llink_cap[e])
    prob += lpSum(tmp_list)

    # Upper layer link capacity constraints
    for link in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*m_mod*ulink_cap[link])
        for d in demand_list:
            for p in range(len(upaths[d])):
                if link_in_path(link, upaths[d][p]):
                    tmp_list.append(udemand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    for link in llink_list:
        tmp_list = []
        tmp_list.append(-1.0*n_mod*llink_cap[link])
        for e in ulink_list:
            d = ul_map[e[0]], ul_map[e[1]]
            for p in range(len(lpaths[d])):
                if link_in_path(link, lpaths[d][p]):
                    tmp_list.append(m_mod*ldemand_paths[e, p])
        link_name = "W{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= 0, "LinkCap|" + link_name

    # upper layer demand satisfaction constraints
    for d in demand_list:
        tmp_list = []
        for p in range(len(upaths[d])):
            tmp_list.append(udemand_paths[d, p])
        constraint_name = "UDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name

    # lower layer demand satisfaction constraints
    for e in ulink_list:
        tmp_list = []
        tmp_list.append(-1.0*ulink_cap[e])
        d = ul_map[e[0]], ul_map[e[1]]
        for p in range(len(lpaths[d])):
            tmp_list.append(ldemand_paths[e, p])
        constraint_name = "LDemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 0, constraint_name
    return prob, udemand_paths, ldemand_paths, ulink_cap, llink_cap

