# Copyright 2014 Dr. Greg M. Bernstein
""" Formulates a version of the access network design problem.
    See course slides for more information.
"""
import networkx as nx
from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum, value
from math import sqrt


def distance(g, nA, nZ):
    """Gets the geometric distance between two nodes.

    Parameters
    ----------
    g : networkx.Graph
        A network graph with node locations given by x and y attributes.
    nA, nZ : string
        Strings representing the two nodes of interest
    """
    xA = g.node[nA]['x']
    xZ = g.node[nZ]['x']
    yA = g.node[nA]['y']
    yZ = g.node[nZ]['y']
    return sqrt((xA - xZ)**2 + (yA - yZ)**2)


def nodeLocationProb(g, node_cost, node_cap, prob_name=None):
    """ Formulates a node location problem for the given network.

    Parameters
    ----------
    g : networkx.Graph
        Problem network; consists only of nodes of two 'types': 'switch' or 'user_region'
    node_cost : dictionary
        A dictionary indexed by nodes of type 'switch' representing the access node costs.
    node_cap : dictionary
        A dictionary indexed by nodes of type 'switch' representing the access node capacity.

    Returns
    -------
    prob, user_access, location : tuple
        A tuple of (prob, user_access, location). Where prob is a PuLP LP problem instance ready to be solved
        or printed, user_access is a dictionary of the user-access link indicator variables, and location
        is a dictionary of access node use indicator variables.

    """
    users = []
    access = []
    for n in g.nodes():
        if g.node[n]['type'] == 'switch':
            access.append(n)
        if g.node[n]['type'] == 'user_region':
            users.append(n)
    users.sort()
    access.sort()
    # compute distance coefficients
    dist = {}
    for u in users:
        for a in access:
            dist[u, a] = distance(g, u, a)

    #print users
    #print access
    print "average distance = {}".format(sum(dist.values())/float(len(dist)))

    if prob_name is None:
        prob_name = "Node Location Design problem"
    prob = LpProblem(prob_name, LpMinimize)
    # Create a dictionary to hold user-access indicator variables
    user_access = {}
    for u in users:
        for a in access:
            name = "u{}_{}".format(u, a)
            user_access[u, a] = LpVariable(name, 0, cat='Binary')
    # Create another dictionary to hold the location variables
    location = {}
    for a in access:
        name = "r_{}".format(a)
        location[a] = LpVariable(name, 0, cat="Binary")
    # Add the objective function which is all links used times their distance
    # plus the sum of the costs of all access nodes.
    tmp_list = []
    for u in users:
        for a in access:
            # m[1] is the cost of the module
            tmp_list.append(dist[u, a]*user_access[u, a])
    for a in access:
        tmp_list.append(node_cost[a]*location[a])
    prob += lpSum(tmp_list)
    # Now lets include the location capacity constraints
    for a in access:
        tmp_list = []
        tmp_list.append(-node_cap[a]*location[a])
        for u in users:
            tmp_list.append(user_access[u, a])
        prob += lpSum(tmp_list)<= 0, "NodeCap" + a

    # Now for the user connectivity satisfaction constraints
    for u in users:
        tmp_list = []
        for a in access:
            tmp_list.append(user_access[u, a])
        constraint_name = "UConnect_{}".format(u)
        prob += lpSum(tmp_list) == 1.0, constraint_name
    return prob, user_access, location




