# Copyright 2014 Dr. Greg M. Bernstein
""" A helper function and a main script to run and solve link placement design problems.
    See course slides for more info.
"""
from Utilities.utilities import gen_cand_paths, gen_rand_demands
from networkx.readwrite import json_graph
import random
from TopologyLinkDesign import topoLinkDesign, topoLinkBoundedCapEx
import operator
from pulp import LpStatus, value
import json

def create_sol_graph(g, link_cap):
    """ Creates a solution network based.

        Parameters
        ----------
        g : networkx.Graph
            the original network
        link_cap : dictionary
            link capacity solution variables from the TopologyLinkDesign problem.

        Returns
        -------
        g_sol : networkx.Graph
            a graph representing the solution network with only allocated links installed.
    """
    gsol = g.copy()  # Make a copy to display solution network
    gsol.remove_edges_from(g.edges())
    for e in link_cap.keys():
        if link_cap[e].varValue > 0.001: #  Not a good numeric test, should mae this relative to demands
            gsol.add_edge(e[0], e[1], capacity=link_cap[e].varValue, weight=g[e[0]][e[1]]['weight'])
    return gsol

if __name__ == '__main__':
    # open up networks, set node costs and capacity formulate problem and solve...
    g = json_graph.node_link_graph(json.load(open("TopoLinkNetExample.json")))
    d_nodes1 = ['B2', 'B5', 'B8', 'B14']
    demand1 = {('B5', 'B14'): 17.952873525048123, ('B8', 'B14'): 26.753927562920648,
               ('B14', 'B2'): 26.441323265622334, ('B5', 'B2'): 25.105056247051415,
               ('B5', 'B8'): 18.432018000879417}
    opex = {}
    capex = {}
    random.seed(6591)  # Set seed for reproducibility
    # note that opex is per volume unit
    for e in g.edges():
        opex[e] = 0.01*g[e[0]][e[1]]['weight']  # For initial problem make opex small compared to capex
        capex[e] = random.randint(3, 12)*g[e[0]][e[1]]['weight']
    paths = gen_cand_paths(g, demand1, 6)
    # Neat way to get a list of links sorted by their capex or opex costs
    # from: https://stackoverflow.com/questions/613183/python-sort-a-dictionary-by-value
    sorted_capex = sorted(capex.iteritems(), key=operator.itemgetter(1))
    sorted_opex = sorted(opex.iteritems(), key=operator.itemgetter(1))


    prob, demand_paths, link_cap, link_use = topoLinkDesign(g, demand1, paths, capex, opex)
    prob.solve()
    print "Problem 1 Status:", LpStatus[prob.status], " Objective: ", value(prob.objective)
    cost1 = prob.objective.value()
    gsol1 = create_sol_graph(g, link_cap)
    #json_graph.dump(gsol1, open("Ex1NetSol1.json","w"))
    for e in g.edges():
        opex[e] = 1.0*g[e[0]][e[1]]['weight']  # For initial problem make opex small compared to capex
    bound = 1.0*cost1
    print "CapEx bound = {}".format(bound)
    prob2, demand_paths2, link_cap2, link_us2e = topoLinkBoundedCapEx(g, demand1, paths, capex, opex, bound)
    prob2.solve()
    print "Problem 2 Status:", LpStatus[prob2.status], " Objective: ", value(prob2.objective)
    gsol2 = create_sol_graph(g, link_cap2)
    json.dump(json_graph.node_link_data(gsol2), open("Ex1NetSol2x.json","w"))







