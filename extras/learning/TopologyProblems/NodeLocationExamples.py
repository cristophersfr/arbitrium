# Copyright 2014 Dr. Greg M. Bernstein
""" Helper function and a main script to formulate and run the node location design problem.
    Used to generate examples and solutions shown in slides.
"""
from networkx.readwrite import json_graph
from NodeLocationDesign import nodeLocationProb
import json

def get_node_count(g, type='switch'):
    """ Returns the number of nodes of a specific type in the network.

    Parameters
    ----------
    g : networkx.Graph
        A graph consisting of nodes only. The nodes have a type attribute.
        This function counts the number with a specific attribute.
    type : string
        The type attribute of the nodes to be counted.

    Returns
    -------
    count : integer
    """
    count = 0
    for n in g.nodes():
        if g.node[n]['type'] == type:
            count += 1
    return count


def get_node_list(g, type='switch'):
    """ Returns a list of nodes of a specific type in the network.

    Parameters
    ----------
    g : networkx.Graph
        A graph consisting of nodes only. The nodes have a type attribute.
        This function counts the number with a specific attribute.
    type : string
        The type attribute of the nodes to be counted.

    Returns
    -------
    n_list : list of nodes
    """
    n_list = []
    for n in g.nodes():
        if g.node[n]['type'] == type:
            n_list.append(n)
    n_list.sort()
    return n_list


def create_g_sol(g, user_access):
    """ Creates a graph of the solution to the node location problem.

    Parameters
    ----------
    g : networkx.Graph
        The problem graph consisting of only nodes.
    user_access : dictionary
        Dictionary of PuLP problem variables representing possible user to
        access links.

    """
    gsol = g.copy()
    for ua in user_access.keys():
        if user_access[ua].varValue >= 0.5:
            gsol.add_edge(ua[0], ua[1])
    return gsol


if __name__ == '__main__':
    # open up networks, set node costs and capacity formulate problem and solve...
    g = json_graph.node_link_graph(json.load(open("accessProb1.json")))
    a_nodes = get_node_list(g)
    # Set up cost and capacity dictionaries
    cap = {}
    cost = {}
    for n in a_nodes:  # Making these very simple and uniform
        cap[n] = 6
        cost[n] = 500
    # formulate the problem
    prob, user_access, location = nodeLocationProb(g, cost, cap, prob_name="NodeLocationTestProb1")
    prob.solve()
    gsol = create_g_sol(g, user_access)



