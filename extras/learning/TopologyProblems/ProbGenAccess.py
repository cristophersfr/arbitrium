# Copyright 2014 Dr. Greg M. Bernstein
""" Routines to help in generating access network node placement problems.
"""
import networkx as nx
import random
import json


def gen_access_graph(n_user, n_nodes, x_limits, y_limits):
    """ Generates an access network design problem graph.
        User areas and access nodes are randomly placed within the
        given bounds.

        Parameters
        ----------
        n_user : integer
            Number of user areas.
        n_nodes : integer
            Number of access nodes
        x_limits, y_limits : tuple
            Float tuples of the bounds on x and y given as (x_low, x_high) and similarly for y.

        Returns
        -------
        g : networkx.Graph
            A graph with only nodes. Each node has attributes 'type', 'x', and 'y'. Where
            type can be either 'user_region' or 'switch'.
    """
    g = nx.Graph()
    for n in range(n_user):
        x = random.uniform(x_limits[0], x_limits[1])
        y = random.uniform(y_limits[0], y_limits[1])
        g.add_node("u{}".format(n), x=x, y=y, type='user_region')
    for n in range(n_nodes):
        x = random.uniform(x_limits[0], x_limits[1])
        y = random.uniform(y_limits[0], y_limits[1])
        g.add_node("n{}".format(n), x=x, y=y, type='switch')
    return g

if __name__ == '__main__':
    from networkx.readwrite import json_graph
    g = gen_access_graph(30, 10, (50, 750), (20, 450))
    json.dump(json_graph.node_link_data(g), open("accessProb1x.json", 'w'))

