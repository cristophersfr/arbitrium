# Copyright 2014 Dr. Greg M. Bernstein
""" Formulation functions for two fairly similar topology link placement design problems.
    See course slides for more explanations and graphics.
"""
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum, value
from Utilities.utilities import link_in_path


def topoLinkDesign(g, demands, paths, link_capex, link_opex, M=1.0e6, prob_name=None):
    """
        A function to formulate a topology link design problem.
        This seeks to minimize both CapEx and OpEx.

        Parameters
        ----------
        g : networkx.Graph
            undirected graph representing the network and its potentially installed links.
        demands : dictionary
            A dictionary indexed by demand node pairs whose value is the demand volume.
        paths : dictionary
            A dictionary of candidate paths indexed by node demand pairs whose value is a list
            of paths. Each path, in turn, is given as a list of nodes.
        link_capex, link_opex : dictionary
            Dictionaries indexed by links (node pairs) which contain the CapEx and OpEx
            cost for each link respectively.
        prob_name : string
            Name that you may want to give to the formulated MILP problem.

        Returns
        -------
        problem tuple : tuple
            A tuple of (prob, demand_path, link_cap, link_used) variables where: prob is the MILP
            problem from PuLP, demand_path is a dictionary of demand path variables, link_cap is a
            dictionary of link capacity variables, and link_used is a dictionary of 'link use' indicator
            variables.

    """
    # Set up Node Link Formulation Linear Program
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    # print demand_list
    # print link_list
    # print node_list
    if prob_name == None:
        prob_name = "Topology Link Design in Link Path Formulation"
    prob = LpProblem(prob_name, LpMinimize)
    # Create a dictionary to hold demand path variables
    # these will be indexed by a tuple of (demand, path_num) pairs.
    demand_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            demand_paths[d, p] = LpVariable(name, 0)
    # Create another dictionary to hold the link capacity
    # variables:
    link_cap = {}
    for e in link_list:
        name = "y_{}_{}".format(e[0], e[1])
        link_cap[e] = LpVariable(name, 0, cat="Continuous")
    # Create a dictionary of link_use binary variables
    link_use = {}
    for e in link_list:
        name = "u_{}_{}".format(e[0], e[1])
        link_use[e] = LpVariable(name, 0, cat="Binary")
    # Add the objective function
    tmp_list = []
    for e in link_list:
        tmp_list.append(link_opex[e]*link_cap[e])
        tmp_list.append(link_capex[e]*link_use[e])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each link
    for link in link_list:
        tmp_list = []
        tmp_list.append(-1.0*link_cap[link])
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkCap|" + link_name
    # Link existence constraints
    for link in link_list:
        tmp_list = []
        tmp_list.append(link_cap[link])
        tmp_list.append(-1.0*M*link_use[link])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkExist|" + link_name
    # Now for the demand satisfaction constraints
    # We one for demand pair.
    for d in demand_list:
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(demand_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name
    return prob, demand_paths, link_cap, link_use

def topoLinkBoundedCapEx(g, demands, paths, link_capex, link_opex, bound, m_big=1.0e6, prob_name=None):
    """
        A function to formulate a topology link design problem.
        This seeks to minimize both CapEx and OpEx.

        Parameters
        ----------
        g : networkx.Graph
            undirected graph representing the network and its potentially installed links.
        demands : dictionary
            A dictionary indexed by demand node pairs whose value is the demand volume.
        paths : dictionary
            A dictionary of candidate paths indexed by node demand pairs whose value is a list
            of paths. Each path, in turn, is given as a list of nodes.
        link_capex, link_opex : dictionary
            Dictionaries indexed by links (node pairs) which contain the CapEx and OpEx
            cost for each link respectively.
        prob_name : string
            Name that you may want to give to the formulated MILP problem.

        Returns
        -------
        problem tuple : tuple
            A tuple of (prob, demand_path, link_cap, link_used) variables where: prob is the MILP
            problem from PuLP, demand_path is a dictionary of demand path variables, link_cap is a
            dictionary of link capacity variables, and link_used is a dictionary of 'link use' indicator
            variables.

    """
    # Set up Node Link Formulation Linear Program
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    # print demand_list
    # print link_list
    # print node_list
    if prob_name == None:
        prob_name = "Topology Link Design in Link Path Formulation"
    prob = LpProblem(prob_name, LpMinimize)
    # Create a dictionary to hold demand path variables
    # these will be indexed by a tuple of (demand, path_num) pairs.
    demand_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            demand_paths[d, p] = LpVariable(name, 0)
    # Create another dictionary to hold the link capacity
    # variables:
    link_cap = {}
    for e in link_list:
        name = "y_{}_{}".format(e[0], e[1])
        link_cap[e] = LpVariable(name, 0, cat="Continuous")
    # Create a dictionary of link_use binary variables
    link_use = {}
    for e in link_list:
        name = "u_{}_{}".format(e[0], e[1])
        link_use[e] = LpVariable(name, 0, cat="Binary")
    # Add the objective function
    tmp_list = []
    for e in link_list:
        tmp_list.append(link_opex[e]*link_cap[e])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each link
    for link in link_list:
        tmp_list = []
        tmp_list.append(-1.0*link_cap[link])
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkCap|" + link_name
    # CapEx bounding constraint
    tmp_list = []
    for link in link_list:
        tmp_list.append(link_capex[link]*link_use[link])
    prob += lpSum(tmp_list) <= bound, "CapExBound"
    # Link existence constraints
    for link in link_list:
        tmp_list = []
        tmp_list.append(link_cap[link])
        tmp_list.append(-1.0*m_big*link_use[link])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkExist|" + link_name
    # Now for the demand satisfaction constraints
    for d in demand_list:
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(demand_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name
    return prob, demand_paths, link_cap, link_use



