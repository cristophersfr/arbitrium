# Copyright 2014 Dr. Greg M. Bernstein
""" Functions to compute and plot the Erlang loss formula.
    See: https://en.wikipedia.org/wiki/Erlang_%28unit%29#Erlang_B_formula
    for the iterative method used here.
    For a full derivation see Zukerman, "Queueing Theory and Stochastic
    Teletraffic Models", section 8.3, page 132.
"""
import numpy as np
import matplotlib.pyplot as plt
from math import log10

def erlangB(A, k):
    """ Erlang B formula
        Also known as the Erlang loss formula

        Parameters
        ----------
        A : float
            offered load. Should be less than `k`
        k : integer
            number of servers or "trunks"

        Returns
        -------
        float
    """
    invB = 1.0
    for m in xrange(1,k+1):
        invB = 1.0 + (m/A)*invB
    return 1.0/invB

def erlangBPlot():
    """ Plots the Erlang B formula
    Uses a fixed number of servers, but you can change this

    Returns
    -------
    A nice matplotlib plot
    """
    x = np.linspace(0.01, 22, 100)
    y = [log10(erlangB(xi, 24)) for xi in x]
    plt.plot(x, y)
    plt.axis([0, 24, -20, 0])
    plt.xlabel('A')
    plt.ylabel('$log_{10}(P_B)$')
    plt.title("Erlang B formula $k=24$")
    plt.show()