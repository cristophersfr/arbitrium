# Copyright 2014 Dr. Greg M. Bernstein
""" Simulation model for an M/M/1/N finite buffer queue based on SimPy. Estimates queue sizes,
    wait times and packet loss and compares to theory.  Vary number of packets sent, inter-arrival
    and service time via parameters in the main portion of the script.

    This code uses global (module) level variables and hence is not very extensible, nor
    an example of good OO design.
"""
import random
import simpy


class Packet(object):
    """ A very simple class that represents a packet.
        This packet will run through a queue at a switch output port.

        Parameters
        ----------
        time : float
            the time the packet arrives at the output queue.
        id : int
            an identifier for the packet (not used for anything yet)
        src, dst : int
            identifiers for source and destination (not used yet)
    """
    def __init__(self, time, id, src="a", dst="z"):
        self.time = time
        self.id = id
        self.src = src
        self.dst = dst


def packet_generator(numPackets, env, out_pipe):
    """A generator function for creating packets.
         Generates packets with exponentially varying inter-arrival times and
         placing them in an output queue.

         Parameters
         ----------
        numPackets : int
            number of packets to send.
        env : simpy.Environment
            The simulation environment.
        out_pipe : simpy.Store
            the output queue model object.
    """
    global queue_size, dropped
    for i in xrange(numPackets):
        # wait for next transmission
        yield env.timeout(random.expovariate(1/ARRIVAL))  # exponential inter-arrival times
        #print "Sending packet {} at time {}".format(i, env.now)
        p = Packet(env.now, i)
        tmp = len(out_pipe.items)
        queue_size += tmp
        if tmp == BUFF_SIZE:
            dropped += 1
        else:
            yield out_pipe.put(p)
        
        
def packet_consumer(env, in_pipe):
    """ A generator function which consumes packets.
        Consumes packets from the packet queue, i.e., models sending a packet of exponentially
        varying size over a link.

    Parameters
    ----------
    env : simpy.Environment
        the simulation environment.
    in_pipe : simpy.Store
        the FIFO model object where packets are kept.
    """
    global queue_wait, total_wait
    while True:
        # Get event for message pipe
        msg = yield in_pipe.get()
        queue_wait += env.now - msg.time
        yield env.timeout(random.expovariate(1/SERVICE))  # exponential service times.
        total_wait += env.now - msg.time
        #print "at time {} processed packet: {} ".format(env.now, msg.id)


def probs(rho, i, N):
    """ Computes the probability of the system having i states occupied.

        Parameters
        ----------
            rho: float
                a value greater than 0 and less than 1.

            N: integer
                is the system capacity, which is the buffer size plus one.
    """
    return (rho**i)*(1.0-rho)/(1.0 - rho**(N+1))


def mean_occupation(rho, N):
    """ Computes the mean of the number of items (packets)currently in the system.
        This includes both items in the queue and items in service.

        Parameters
        ----------
            N: integer
                the total system capacity.
    """
    sum = 0.0
    for i in range(N+1):
        sum += i*probs(rho, i, N)
    return sum

if __name__ == '__main__':
    NUM_PACKETS = 50000  # number of packets we'll run through the system
    ARRIVAL = 3.0  # mean inter-arrival times (not rate)
    SERVICE = 1.7  # mean service time (not rate)
    BUFF_SIZE = 6  # Buffer size, not including item that can be in service

    # Counters to keep track of waiting times, queue sizes, packets dropped, etc...
    queue_wait = 0
    total_wait = 0
    queue_size = 0
    ## Counter to keep track of dropped packets.
    dropped = 0
    # Setup and start the simulation
    RANDOM_SEED = 42

    #random.seed(RANDOM_SEED)

    ## The simulation environment.
    env = simpy.Environment()
    ## The switch output port object based on the SimPy Store class
    pipe = simpy.Store(env)
    # Turns our generator functions into SimPy Processes
    env.process(packet_generator(NUM_PACKETS, env, pipe))
    env.process(packet_consumer(env, pipe))

    print 'A  M/M/1/N queueing simulation'
    env.run()  # Yup. This runs the simulation

    print "Ending simulation time: {}".format(env.now)
    # Formulas from Klienrock, "Queueing Systems, Volume I:Theory", 1975.
    mu = 1.0/SERVICE
    l = 1.0/ARRIVAL
    rho = l/mu
    ## average wait in an M/M/1 queue
    W = rho/mu/(1-rho)
    ## average total system time in an M/M/1.
    T = 1/mu/(1-rho)
    ## The average number waiting in the M/M/1 queue
    nq_bar = rho/(1.0 - rho) - rho

    print "M/M/1 Theory: avg queue wait {}, avg total time {}, avg queue size {}".format(W, T, nq_bar)
    dropTheory = pow(rho, BUFF_SIZE+1)*(1.0 - rho)/(1.0 - pow(rho, BUFF_SIZE + 2))
    print 'M/M/1/N Dropped theory = {}'.format(probs(rho, BUFF_SIZE+1, BUFF_SIZE+1))
    mean_system = mean_occupation(rho, BUFF_SIZE+1)
    mean_service = 1.0 - probs(rho, 0, BUFF_SIZE+1)
    print 'M/M/1/N Theory system occupation = {}'.format(mean_system)
    print 'M/M/1/N Theory mean in service = {}, mean in queue = {}'.format(mean_service, mean_system-mean_service)

    print 'Sim Average queue wait = {}'.format(queue_wait/NUM_PACKETS)
    print 'Sim Average total wait = {}'.format(total_wait/NUM_PACKETS)
    print 'Sim Average queue size = {}'.format(queue_size/float(NUM_PACKETS))
    print 'Dropped packets = {} Fraction dropped = {}'.format(dropped, dropped/float(NUM_PACKETS))



