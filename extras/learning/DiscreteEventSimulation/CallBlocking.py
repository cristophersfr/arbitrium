# Copyright 2014 Dr. Greg M. Bernstein
""" A call blocking simulation example.

    Here we simulate an M/M/k/k queue and check the results against the Erlang loss formula.

    In this example we take a more OO approach and do not use global variables except for the running of the
    main simulation.
"""
__author__ = 'Greg Bernstein'
import random
import simpy
from erlang import erlangB


class CallGenerator(object):
    """ Generates calls with exponential inter-arrival times.

        Parameters
        ----------
        env : simpy.Environment
            the simulation environment
        arrival_time : float
            the inter-arrival times of calls to the server resource.
        service_time : float
            the mean holding time of the calls.
        server : simpy.Resource
            the simpy.Resource representing the "trunk" or shared resource pool


    """
    def __init__(self, env, arrival_time, service_time, server):
        self.env = env
        self.server = server
        self.a_time = arrival_time
        self.s_time = service_time
        self.capacity = server.capacity
        ## call count information
        self.call_count = 0
        ## block call information
        self.call_blocked = 0

    def run(self):
        """The generator function used in simulations.
        """
        while True:
            yield self.env.timeout(random.expovariate(1.0/self.a_time))
            self.call_count += 1
            if self.server.count == self.capacity:
                #print 'Dropping a call at time {}'.format(self.env.now)
                self.call_blocked += 1
            else:
                #print 'Adding call at time {}, number in service = {}'.format(self.env.now, self.server.count)
                self.env.process(call_process(self.env, self.server, self.s_time))
                # Create and start call process here and feed it the env, and server


def call_process(env, resource, service_time):
    """ A model for a call.

        The call takes and holds a resource for an exponentially distributed service time.

        Parameters
        ----------
        env : simpy.Environment
            the simulation environment
        resource : simpy.Resource
            the simpy.Resource the call will utilize
        service_time : float
            the mean service time
    """
    # The following commented out code will work, but the non-commented out code
    # is the recommended way to take and release a resource in SimPy.
    # req = yield resource.request()
    # yield env.timeout(random.expovariate(1.0/service_time))
    # yield resource.release(req)
    with resource.request() as request:
        yield request
        #yield env.timeout(random.expovariate(1.0/service_time))
        yield env.timeout(service_time)
        # Using the with statement automatically releases the resource when we are done.
        #print 'Removing a call from the server at time {}'.format(env.now)

if __name__ == '__main__':
    # main program
    ## The offered load in Erlangs
    A = 17.0 * 1.3  #  Assignment #2 problem 5
    ## The number of "server" or trunk resources
    k = 25          #  Assignment #2 problem 5
    ## The simulation environment.
    env = simpy.Environment()
    ## The "resource" pool
    server = simpy.Resource(env, capacity=k)
    ## A call generator object
    call_gen = CallGenerator(env, 1.0/17.0, 1.3, server)
    env.process(call_gen.run())
    env.run(until=5000.0)
    print 'Total calls = {}, Dropped calls = {}, Blocking Probability = {}'.\
        format(call_gen.call_count, call_gen.call_blocked, float(call_gen.call_blocked)/call_gen.call_count)
    print 'Theoretical call blocking = {}'.format(erlangB(A, k))