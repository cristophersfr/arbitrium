# Copyright 2014 Dr. Greg M. Bernstein
""" A single script that formulates a continuous **link-path** dimensioning problem.
    This was used to generate the example given in the course slides. This is
    not designed to be reusable since the "shortest path allocation rule" makes
    this approach highly inefficient.
"""
from Utilities.utilities import link_in_path, gen_cand_paths
from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum, value
import json
from Utilities.YenKShortestPaths import YenKShortestPaths

if __name__ == '__main__':
    # Get the network from a JSON file
    g = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    print g.edges(data=True)
    # Here we define demands using a Python dictionary. A Python tuple (a,b)
    # is used for the demand pair, and its dictionary value represents its
    # volume.
    demands = {("N0", "N3"): 25, ("N2", "N3"): 29,
               ("N1", "N6"): 25, ("N3", "N5"): 31,
               ("N0", "N2"): 16}
    print demands
    # Generate Demand path candidates
    paths = gen_cand_paths(g, demands, 3)
    print paths
    # Set up Node Link Formulation Linear Program
    # First some nice lists to help us stay organized
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    node_list = sorted(g.nodes())
    print demand_list
    print link_list
    print node_list
    prob = LpProblem("Basic Dimensioning in Link Path Formulation", LpMinimize)
    # Create a dictionary to hold demand path variables
    # these will be indexed by a tuple of (demand, path_num) pairs.
    demand_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            demand_paths[d, p] = LpVariable(name, 0)
    # Create another dictionary to hold the link capacity variables:
    link_cap = {}
    for e in link_list:
        name = "y_{}_{}".format(e[0], e[1])
        link_cap[e] = LpVariable(name, 0)
    # Add the objective function which is the cost of
    # all links times their capacity.
    tmp_list = []
    for e in link_list:
        tmp_list.append(g[e[0]][e[1]]["weight"]*link_cap[e])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each lin
    for link in link_list:
        tmp_list = []
        tmp_list.append(-1.0*link_cap[link])
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkCap|" + link_name
    # Now for the demand satisfaction constraints, one for demand pair.
    for d in demand_list:
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(demand_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name

    prob.writeLP("basicDimeLinkPathEx1.lpt")
    prob.solve()
    print "Status:", LpStatus[prob.status], " Objective: ", value(prob.objective)
    for v in prob.variables():
        print v.name, "=", v.varValue