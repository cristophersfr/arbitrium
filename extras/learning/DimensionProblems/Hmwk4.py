""" A script to aid in the solution of homework #4 examples.
    Note that the "network B" problem generates a challenging MIP problem
    so this script does not call the solvers.
"""
from networkx.readwrite import json_graph
import Utilities.jsonconverter as jc
import json
from MultiModDimLinkPathFormulation import multiModDim
from Utilities.utilities import gen_cand_paths


if __name__ == '__main__':
    # Get networks
    netA = json_graph.node_link_graph(json.load(open("NetAHmwk4.json")))
    netB = json_graph.node_link_graph(json.load(open("NetBHmwk4.json")))
    # Get demands
    demandsA = jc.j_to_demands(json.load(open("demandsA_hmwk4.json")))
    demandsB = jc.j_to_demands(json.load(open("demandsB_hmwk4.json")))
    # Get Candidate Paths
    pathsA = gen_cand_paths(netA, demandsA, 4)
    pathsB = gen_cand_paths(netB, demandsB, 7)
    link_mod = [(1.0,1.0), (10.0, 5.0)]  # (capacity, cost)
    probA, d_pathsA, lin_capA = multiModDim(netA, demandsA, pathsA, link_mod, prob_name="NetAEth")
    probB, d_pathsB, lin_capB = multiModDim(netB, demandsB, pathsB, link_mod, prob_name="NetBEth")
