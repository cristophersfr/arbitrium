# Copyright 2014 Dr. Greg M. Bernstein
""" A single script that formulates a modular **node-link** dimensioning problem.
    This was used to generate the example given in the course slides. This generates
    a mixed integer programming (MIP) problem.
"""
from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum, value
import json


if __name__ == '__main__':
    # modularity of link allocations
    link_mod = 40
    # Define the network, we'll start with an undirected version and use a
    # convenient utility function to convert it to a directed graph.
    # Get the network from a JSON file
    g_temp = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    g = g_temp.to_directed()  # Nice function to produce a directed version
    print g.edges(data=True)
    # Here we define demands using a Python dictionary. A Python tuple (a,b)
    # is used for the demand pair, and its dictionary value represents its
    # volume.
    demandsUni = {("N0", "N3"): 25, ("N2", "N3"): 29,
               ("N1", "N6"): 25, ("N3", "N5"): 31,
               ("N0", "N2"): 16}
    demands = {}
    for d in demandsUni.keys():
        demands[d] = demandsUni[d]
        demands[d[1],d[0]] = demandsUni[d]
    print demands
    # Set up Node Link Formulation Linear Program
    # First some nice lists to help us stay organized
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    node_list = sorted(g.nodes())
    print demand_list
    print link_list
    print node_list
    prob = LpProblem("Dimensioning Basic Node Link Formulation", LpMinimize)
    # Create a dictionary to hold link flow variables
    # these will be indexed by a tuple of (link, demand) pairs.
    # remember that these are each tuples themselves
    link_flows = {}
    for link in link_list:
        for demand in demand_list:
            name = "xL{}_{}_D{}_{}".format(link[0], link[1], demand[0], demand[1])
            link_flows[(link,demand)] = LpVariable(name, 0)

    # Create another dictionary to hold the link capacity
    # variables:
    link_cap = {}
    for e in link_list:
        name = "y_{}_{}".format(e[0], e[1])
        link_cap[e] = LpVariable(name, 0, cat="Integer")  # Capacity variables constrained to integers
    # Add the objective function which is the cost of
    # all links times their capacity.
    tmp_list = []
    for e in link_list:
        tmp_list.append(link_mod*g[e[0]][e[1]]["weight"]*link_cap[e])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each link
    for link in link_list:
        tmp_list = []
        tmp_list.append(-link_mod*link_cap[link])  # In terms of link modularity
        for demand in demand_list:
            tmp_list.append(link_flows[(link, demand)])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkCap|" + link_name
    # Now for the node flow conservation constraints
    # We have one of these for each node and demand pair.
    for node in node_list:
        for demand in demand_list:
            tmp_list = []
            in_edges = g.in_edges(node)
            out_edges = g.out_edges(node)
            for link in out_edges:
                tmp_list.append(link_flows[(link, demand)])
            for link in in_edges:
                tmp_list.append(-1.0*link_flows[(link, demand)])
            rhs = 0.0  # Default transit node, but check if source or sink
            if node == demand[0]:  # Is node the source of the demand
                rhs = demands[demand]
            if node == demand[1]:  # Is node the sink of the demand
                rhs = -demands[demand]
            constraint_name = "NodeCons|{}D{}_{}".format(node, demand[0], demand[1])
            prob += lpSum(tmp_list) == rhs, constraint_name
    prob.writeLP("ModDimNodeLink.lpt")
    prob.solve()
    print "Status:", LpStatus[prob.status], " Objective: ", value(prob.objective)
    for v in prob.variables():
        print v.name, "=", v.varValue

