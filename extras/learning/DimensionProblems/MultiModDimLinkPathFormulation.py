# Copyright 2014 Dr. Greg M. Bernstein
""" Formulation of a multi-modular **link-path** dimensioning problem.
    This was used to solve the example given in the course slides and assignment #4.
    This generates a mixed integer programming (MIP) problem.
"""
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum, value
from Utilities.utilities import link_in_path


def multiModDim(g, demands, paths, link_mod, prob_name=None):
    """ Formulate a multi-modular dimensioning problem with a link path formulation.
        This formulation allows for path splitting. The problem is returned in an
        unsolved state.

        Parameters
        ----------
        g : networkx.Graph
            an undirected graph
        demands : dictionary
            the demand dictionary (indexed by a node pair)
        paths : dictionary
            a dictionary of candidate path lists (index by node pairs)
        link_mods : list
            a list of link module (size, cost) tuples

        Returns
        -------
        prob, demand_paths, link_cap : pulp.LpProblem, dictionary, dictionary
            the MILP problem, the dictionary of demand_path variables, and the
            dictionary of modular link_cap variables.
    """
    # Set up Node Link Formulation Linear Program
    # First some nice lists to help us stay organized
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    node_list = sorted(g.nodes())
    # print demand_list
    # print link_list
    # print node_list
    if prob_name == None:
        prob_name = "Multi Module Dimensioning in Link Path Formulation"
    prob = LpProblem(prob_name, LpMinimize)
    # Create a dictionary to hold demand path variables
    # these will be indexed by a tuple of (demand, path_num) pairs.

    demand_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            demand_paths[d, p] = LpVariable(name, 0)

    # Create another dictionary to hold the link capacity
    # variables:
    link_cap = {}
    for e in link_list:
        for i, m in enumerate(link_mod):
            name = "y_{}_{}_M_{}".format(e[0], e[1], i)
            link_cap[e, i] = LpVariable(name, 0, cat="Integer")  #  Restricting variable to integer
    # Add the objective function which is the cost of
    # all links times their capacity.
    tmp_list = []
    for e in link_list:
        for i, m in enumerate(link_mod):
            # m[1] is the cost of the module
            tmp_list.append(m[1]*g[e[0]][e[1]]["weight"]*link_cap[e, i])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each lin
    for link in link_list:
        tmp_list = []
        for i, m in enumerate(link_mod):
            # m[0] is the capacity of the module
            tmp_list.append(-m[0]*link_cap[link,i])
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= 0, "LinkCap|" + link_name

    # Now for the demand satisfaction constraints
    # We one for demand pair.
    for d in demand_list:
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(demand_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name
    return prob, demand_paths, link_cap




