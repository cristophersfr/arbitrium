# Copyright 2014 Dr. Greg M. Bernstein
"""
    This file shows how to generate the MIP problem for the Single Path
    capacitated network design problem (multi-commodity routing) in the
    node-link formulation.
"""
import sys, os
sys.path.insert(0, os.path.abspath('../Utilities/'))
from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum
import Utilities.jsonconverter as jc
import json
import Utilities.flow_paths as fp



def single_path_capacitated_node_link(g, demands, nd=None):
    """ Creates a single path capacitated network design problem in a node-link formulation

        Parameters
        ----------
        g : networkx.DiGraph
            a directed network graph g,
        demands : dictionary
            a dictionary of demands indexed by node pairs
        nd : dictionary
            (optional) nd if given is a dictionary of hop count limits for each demand.
        Returns
        -------
        pulp.LpProblem
            a PuLP problem instance suitable for solving or writing. Note that this is now
            a Mixed Integer Program containing binary and continuous variables.
    """
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    node_list = sorted(g.nodes())
    prob = LpProblem("Basic Node Link Formulation", LpMinimize)
    unit_flows = {}
    for link in link_list:  # link flow variables indexed by a tuple of (link, demand) pairs.
        for demand in demand_list:
            name = "xL{}_{}_D{}_{}".format(link[0], link[1], demand[0], demand[1])
            unit_flows[(link,demand)] = LpVariable(name, 0, cat='Binary')
    tmp_list = []
    for link in link_list:  # Objective function
        for demand in demand_list:
            tmp_list.append(g[link[0]][link[1]]["weight"]*demands[demand]*unit_flows[(link,demand)])
    prob += lpSum(tmp_list)
    for link in link_list:  #link capacity constraints
        tmp_list = []
        for demand in demand_list:
            tmp_list.append(demands[demand]*unit_flows[(link, demand)])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= g[link[0]][link[1]]['capacity'], "LinkCap|" + link_name
    for node in node_list:  # node flow conservation constraints
        for demand in demand_list:
            tmp_list = []
            in_edges = g.in_edges(node)
            out_edges = g.out_edges(node)
            for link in out_edges:
                tmp_list.append(unit_flows[(link, demand)])
            for link in in_edges:
                tmp_list.append(-1.0*unit_flows[(link, demand)])
            rhs = 0.0  # Default transit node, but check if source or sink
            if node == demand[0]:  # Is node the source of the demand
                rhs = 1.0
            if node == demand[1]:  # Is node the sink of the demand
                rhs = -1.0
            constraint_name = "NodeCons|{}D{}_{}".format(node, demand[0], demand[1])
            prob += lpSum(tmp_list) == rhs, constraint_name
    if nd is not None:
        for d in demand_list:
            tmp_list = []
            for e in link_list:
                tmp_list.append(unit_flows[e, d])
            constraint_name = "HopCount|D{}_{}".format(d[0], d[1])
            prob += lpSum(tmp_list) <= nd[d], constraint_name
    return prob, unit_flows

if __name__ == "__main__":
    # Get the network from a JSON file
    tmp = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    g = tmp.to_directed()
    print g.edges(data=True)
    tmp = jc.j_to_demands(json.load(open("demandSPLinkPathEx1.json")))
    demands = {}
    # create bidirectional demands
    for d in tmp.keys():
        demands[d] = tmp[d]
        demands[d[1],d[0]] = tmp[d]
    # Try setting up some hop count limits for the demands
    nd = {}
    for d in demands.keys():
        nd[d] = 4  # Default test hop count limit
    nd['N1', 'N6'] = 2 # Limit this
    nd['N6', 'N1'] = 2 # and this
    prob, flow_vars = single_path_capacitated_node_link(g, demands, nd)
    prob.writeLP("SPANodeLink.lpt")
    prob.solve()
    print "Status:", LpStatus[prob.status]
    link_loads = {}
    for e in g.edges():
        tmp_load = 0.0
        for d in demands.keys():
            tmp_load += flow_vars[e,d].value()
        link_loads[e] = tmp_load
    print link_loads
    d_links = fp.getDemandLinks(demands, list(g.edges()), flow_vars)
    print d_links
    d_paths = {}
    all_paths = fp.getAllPathsInfo(demands, d_links, g)

    print json.dumps(all_paths)

