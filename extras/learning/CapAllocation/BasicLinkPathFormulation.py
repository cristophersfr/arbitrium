# Copyright 2014 Dr. Greg M. Bernstein
""" A general link-path formulation of the capacitated network design problem.
"""

from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum
import Utilities.jsonconverter as jc
from Utilities.utilities import link_in_path, pathCost
import json



def basic_link_path(g, demands, paths):
    """ Creates a basic capacitated network design problem with a link-path formulation

        Parameters
        ----------
        g : networkx.DiGraph
            a directed network graph g,
        demands : dictionary
            a dictionary of demands indexed by node pairs
        paths : dictionary
            a dictionary of candidate paths. Index by a node pair and for each node
            pair contains a list of paths, each path a list of nodes.
        Returns
        -------
        pulp.LpProblem
            a PuLP problem instance suitable for solving or writing.
    """
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    node_list = sorted(g.nodes())
    # print demand_list
    # print link_list
    # print node_list
    prob = LpProblem("Basic Link Path Formulation", LpMinimize)
    # Create a dictionary to hold demand path variables
    # these will be indexed by a tuple of (demand, path_num) pairs.
    demand_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            demand_paths[d, p] = LpVariable(name, 0)
    # Add the objective function which for now is the sum of all the
    # demand path variables times their respective path length.
    tmp_list = []
    for d in demand_list:
        for p in range(len(paths[d])):
            tmp_list.append(pathCost(paths[d][p], g)*demand_paths[d, p])
    prob += lpSum(tmp_list)
    # Now lets include the link capacity constraints
    # we have one to add for each link
    for link in link_list:
        tmp_list = []
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demand_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list) <= g[link[0]][link[1]]['capacity'], "LinkCap|" + link_name

    # Now for the demand satisfaction constraints
    # We one for demand pair.
    for d in demand_list:
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(demand_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == demands[d], constraint_name
    return prob, demand_paths


if __name__ == "__main__":
    # Get the network from a JSON file
    g = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    print g.edges(data=True)
    demands = jc.j_to_demands(json.load(open("demandSPLinkPathEx1.json")))
    # Get Demand path candidates
    paths = jc.j_to_paths(json.load(open("paths2LinkPathEx1.json")))
    prob, demand_paths = basic_link_path(g, demands, paths)
    prob.writeLP("basicLinkPathEx1.lpt")
    prob.solve()
    print "Status:", LpStatus[prob.status]
    for v in prob.variables():
        print v.name, "=", v.varValue


