# Copyright 2014 Dr. Greg M. Bernstein
""" This file contains a more elaborate example of a capacitated allocation problem with
    flow splitting. It uses a node link formulation.
"""
import sys, os
sys.path.insert(0, os.path.abspath('../Utilities/'))
from networkx.readwrite import json_graph
from pulp import LpStatus
import Utilities.jsonconverter as jc
import json
from BasicNodeLinkFormulation import basic_capacitated_node_link
import Utilities.flow_paths as fp

if __name__ == '__main__':
    # Get the network from a JSON file
    tmp = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    g = tmp.to_directed()
    print g.edges(data=True)
    tmp = jc.j_to_demands(json.load(open("demandLinkPathEx1.json")))
    demands = {}
    # create bidirectional demands
    for d in tmp.keys():
        demands[d] = tmp[d]
        demands[d[1], d[0]] = tmp[d]
    prob, flow_vars = basic_capacitated_node_link(g, demands)
    prob.solve()
    print "Status:", LpStatus[prob.status]
    link_loads = {}
    for e in g.edges():
        tmp_load = 0.0
        for d in demands.keys():
            tmp_load += flow_vars[e, d].value()
        link_loads[e] = tmp_load
    print link_loads
    d_links = fp.getDemandLinks(demands, list(g.edges()), flow_vars)
    print d_links
    d_paths = {}
    all_paths = fp.getAllPathsInfo(demands, d_links, g)
    print json.dumps(all_paths)
