# Copyright 2014 Dr. Greg M. Bernstein
""" Single Path per Demand Link-Path capacitated allocation problem formulation.
"""


from networkx.readwrite import json_graph
from pulp import LpProblem, LpMinimize, LpVariable, LpStatus, lpSum
import Utilities.jsonconverter as jc
from Utilities.utilities import link_in_path, pathCost
import json


def spa_cap_link_path(g, demands, paths):
    """ Creates a single path capacitated network design problem with a link-path formulation

        Parameters
        ----------
        g : networkx.Graph
            an undirected network graph g,
        demands : dictionary
            a dictionary of demands indexed by node pairs
        paths : dictionary
            a dictionary of candidate paths. Index by a node pair and for each node
            pair contains a list of paths, each path a list of nodes.
        Returns
        -------
        pulp.LpProblem
            a PuLP problem instance suitable for solving or writing. Note that this is now
            a Mixed Integer Program containing binary and continuous variables.
    """
    demand_list = sorted(demands.keys())
    link_list = sorted(g.edges())
    prob = LpProblem("Basic Link Path Formulation", LpMinimize)
    # Create a dictionary to hold unit demand path variables
    unit_paths = {}
    for d in demand_list:
        for p in range(len(paths[d])):
            name = "xD{}_{}P_{}".format(d[0], d[1], str(p))
            unit_paths[d, p] = LpVariable(name, 0, cat='Binary')
    # Add the objective function
    tmp_list = []
    for d in demand_list:
        for p in range(len(paths[d])):
            tmp_list.append(pathCost(paths[d][p], g)*demands[d]*unit_paths[d, p])
    prob += lpSum(tmp_list)
    for link in link_list:  # Now lets include the link capacity constraints
        tmp_list = []
        for d in demand_list:
            for p in range(len(paths[d])):
                if link_in_path(link, paths[d][p]):
                    tmp_list.append(demands[d]*unit_paths[d, p])
        link_name = "L{}_{}".format(link[0], link[1])
        prob += lpSum(tmp_list)<= g[link[0]][link[1]]['capacity'], "LinkCap|" + link_name
    for d in demand_list:  # Now for the demand satisfaction constraints
        tmp_list = []
        for p in range(len(paths[d])):
            tmp_list.append(unit_paths[d, p])
        constraint_name = "DemandSat_{}_{}".format(d[0], d[1])
        prob += lpSum(tmp_list) == 1, constraint_name
    return prob, unit_paths


if __name__ == "__main__":
    # Get the network from a JSON file
    g = json_graph.node_link_graph(json.load(open("linkPathEx1.json")))
    print g.edges(data=True)
    demands = jc.j_to_demands(json.load(open("demandSPLinkPathEx1.json")))
    # Get Demand path candidates
    paths = jc.j_to_paths(json.load(open("pathsLinkPathEx1.json")))
    prob, demand_paths = spa_cap_link_path(g, demands, paths)
    prob.writeLP("SPLinkPathEx1Paths3.lpt")
    prob.solve()
    print "Status:", LpStatus[prob.status]
    for v in prob.variables():
        print v.name, "=", v.varValue


