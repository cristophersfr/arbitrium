# Copyright 2014 Dr. Greg M. Bernstein
""" A simple script plots the Weibull probability density function for various
    values of shape and scale parameters.
"""

from math import exp, pow
import numpy as np
import matplotlib.pyplot as plt

def weibullPdf(x, k, lamb):
    """ Computes the Weibull probability density function

    Parameters
    ----------
    x : float
    k : float
        shape parameter > 0
    lamb : float
        scale parameter > 0

    Returns
    -------
    float
        Value of the pdf exaluated at x
    """
    if x == 0.0 and k < 1.0:
        raise ZeroDivisionError
    if x < 0.0:
        return 0.0
    temp = pow(x/lamb,k)
    temp2 = (k/lamb)*pow(x/lamb, k-1.0)

    return temp2*exp(-temp)

if __name__ == '__main__':
    x = np.linspace(0.01, 2.5, 200)
    y1 = [weibullPdf(xi, 0.5, 1.0) for xi in x]
    y2 = [weibullPdf(xi, 1.0, 1.0) for xi in x]
    y3 = [weibullPdf(xi, 2.0, 1.0) for xi in x]
    y4 = [weibullPdf(xi, 4.0, 1.0) for xi in x]
    fig, axis = plt.subplots()
    axis.plot(x, y1, label="k=0.5")
    axis.plot(x, y2, label="k=1.0")
    axis.plot(x, y3, label="k=2.0")
    axis.plot(x, y4, label="k=4.0")
    axis.set_xlim([0, 2.5])
    axis.set_ylim([0, 2.5])
    axis.set_xlabel('x')
    axis.set_ylabel(r'$p(x,k, \lambda)$')
    axis.legend()
    axis.set_title(r"Weibull pmfs for various $k$s and $\lambda$s")
    fig.savefig("WeibullPmf.svg")
    plt.show()