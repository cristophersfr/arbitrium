# Copyright 2014 Dr. Greg M. Bernstein
""" A simple script that uses the Python *random* library and the *matplotlib*
    library to create histogram of Weibull distributed random numbers.
"""

import random
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Generate a list of samples
    # This technique is called a "list comprehension" in Python
    weibSamples = [random.weibullvariate(1.0, 0.5) for i in xrange(100000)]
    print weibSamples[0:10]  #Take a look at the first 10
    fig, axis = plt.subplots()
    axis.hist(weibSamples, bins=100, normed=True)
    axis.set_title(r"Greg's Histogram of an Weibull RNG $\lambda = 1, k=0.5$")
    axis.set_xlabel("x")
    axis.set_ylabel("normalized frequency of occurrence")
    axis.set_xlim([0,50])
    fig.savefig("WeibullHistogram.png")
    plt.show()
