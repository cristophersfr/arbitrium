# Copyright 2014 Dr. Greg M. Bernstein
""" A simple script to show how to get and use discrete random variable distributions from SciPy."""

import numpy as np
from scipy.stats import binom
import matplotlib.pyplot as plt

if __name__ == '__main__':
    [n, p ] = [20, 0.90]
    print "Creating a Binomial random variable with n = {} and p = {}".format(n,p)
    rv = binom(n, p)
    print "Mean = {} and Variance = {}".format(rv.mean(), rv.var())
    samps = rv.rvs(size=10)  # Generates 10 samples
    print "Some samples: {}".format(samps)
    #Display frozen pmf
    x = np.arange(0, n+2)
    fig, axis = plt.subplots()
    axis.vlines(x, 0, rv.pmf(x), 'b', lw=5)
    axis.set_title("Binomial pmf $n=20$, $p = 0.9$")
    axis.set_ylabel("P(k)")
    axis.set_xlabel("x")
    fig.savefig("BinomialPMF.png")
    plt.show()

