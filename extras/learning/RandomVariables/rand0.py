# Copyright 2014 Dr. Greg M. Bernstein
"""A simple script that uses the Python random library to generate random
   numbers according to a number of different distributions. This alos shows some very simple
   python syntax.
"""

import random
if __name__ == '__main__':
    print "Here is a single sample from a uniform random variable"
    print random.random()
    print "Here is a list of three samples:"
    uniSamples = [random.random(), random.random(), random.random()]
    print uniSamples
    print "Here is a list of three exponential samples:"
    expSamples = [random.expovariate(1.0), random.expovariate(1.0), random.expovariate(1.0)]
    print expSamples
    print "Here is a list of three normal samples:"
    normSamples = [random.normalvariate(0.0, 1.0), random.normalvariate(0.0, 1.0), random.normalvariate(0.0, 1.0)]
    print normSamples





