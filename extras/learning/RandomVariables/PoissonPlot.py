# Copyright 2014 Dr. Greg M. Bernstein
""" A simple script to show how to get and use Poisson and Binomial
    distributions from SciPy. Compares and plots these distributions for
    some interesting parameter values.
"""

import numpy as np
from scipy.stats import binom
from scipy.stats import poisson
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Case one: n = 1.0e10, p=1.0e-12
    # Case two: n = 3600.0e10, p = 1.0e-12
    mu = 0.01
    [n, p] = [1.0e10, 1.0e-12]
    [n2, p2] = [3600.0e10, 1.0e-12]
    mu2 = 36.0
    print "Creating a Poisson random variable with  and mu = {}".format(mu)
    rvp = poisson(mu)
    rvb = binom(n, p)
    rvp2 = poisson(mu2)
    rvb2 = binom(n2, p2)
    print "Mean = {} and Variance = {}".format(rvp.mean(), rvp.var())
    samps = rvp.rvs(size=10)  # Generates 10 samples
    print "Some samples: {}".format(samps)
    #Display frozen pmf
    x = np.arange(0, 10)
    x2 = np.arange(0, 50)

    # Create a 2 x 2 grid of subplots
    fig, axes = plt.subplots(nrows=2, ncols=2)
    axes[0,0].plot(x, rvb.pmf(x), "r--", x, rvp.pmf(x), "g-")
    axes[0,0].set_ylabel("P(k)")
    axes[0,0].set_xlabel("k")
    axes[0,0].set_title("Bit errors in a second")

    axes[0,1].plot(x, rvb.logpmf(x), "--", x, rvp.logpmf(x), "-")
    axes[0,1].set_ylabel("log(P(k))")
    axes[0,1].set_xlabel("k")
    axes[0,1].set_title("Bit errors in a second")

    axes[1,0].plot(x2, rvb2.pmf(x2), "--", label="Binomial")
    axes[1,0].plot(x2, rvp2.pmf(x2), "-", label="Poisson")
    axes[1,0].set_ylabel("P(k)")
    axes[1,0].set_xlabel("k")
    axes[1,0].set_title("Bit errors in an hour")
    axes[1,0].legend(loc=2)

    axes[1,1].plot(x2, rvb2.logpmf(x2),"--", label="Binomial")
    axes[1,1].plot(x2, rvp2.logpmf(x2), "-", label="Poisson")
    axes[1,1].set_ylabel("log(P(k))")
    axes[1,1].set_xlabel("k")
    axes[1,1].set_title("Bit errors in an hour")
    axes[1,1].legend(loc=4)
    fig.tight_layout()
    plt.show()
