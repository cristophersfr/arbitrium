# Copyright 2014 Dr. Greg M. Bernstein
""" A simple script that uses the Python random library and the Matplotlib library to create
    a histogram of uniformly distributed random numbers.
"""

import random
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Generate a list of samples
    # This technique is called a "list comprehension" in Python
    uniSamples = [random.random() for i in xrange(100000)]
    print uniSamples[0:10]  #Take a look at the first 10
    fig, axis = plt.subplots()
    axis.hist(uniSamples, bins=100, normed=True)
    axis.set_title("Histogram for uniform random number generator")
    axis.set_xlabel("x")
    axis.set_ylabel("normalized frequency of occurrence")
    fig.savefig("UniformHistogram.png")
    plt.show()
