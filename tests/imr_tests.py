import unittest
from utils import json_get_req
from config import ONOS_IP, ONOS_PORT

class TestIMR(unittest.TestCase):
    def test_if_ifwd_is_active(self):
        ifwd = 'org.onosproject.ifwd'
        response = json_get_req('http://{0}:{1}/onos/v1/applications/{2}'.format(ONOS_IP, ONOS_PORT, ifwd))
        self.assertEqual(response['state'], 'ACTIVE')

    def test_if_imr_is_active(self):
        imr = 'org.onosproject.imr'
        response = json_get_req('http://{0}:{1}/onos/v1/applications/{2}'.format(ONOS_IP, ONOS_PORT, imr))
        self.assertEqual(response['state'], 'ACTIVE')

    def test_if_imr_get_stats_is_non_empty(self):
        response = json_get_req('http://{0}:{1}/onos/v1/imr/imr/monitoredIntents'.format(ONOS_IP, ONOS_PORT))
        self.assertNotEqual(response['response'], [], msg="IMR is not monitoring any intents")
