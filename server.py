#!/usr/bin/python3
# socket_echo_server_dgram.py
import socket
import struct
import time
import argparse

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process the args')
    parser.add_argument('-sip','--src_ip', help='Insert the source IP', type=str)
    parser.add_argument('-sport','--src_port', help='Insert the source port', type=int)
    args = parser.parse_args()

    # Bind the socket to the port
    server_address = (args.src_ip, args.src_port)
    # print('starting up on {} port {}'.format(*server_address))
    sock.bind(server_address)

    while True:
        # print('\nwaiting to receive message')
        data, address = sock.recvfrom(1024)

        # print('received {} bytes from {} at {}'.format(
        #     len(data), address, time.time()))

        if data:
            # sent = sock.sendto(data, address)
            k, pkt_num = struct.unpack('!HL', data[:6])
            timestamp = time.time()
            print('{0} received_from {1} {2} {3} {4}'.format(
                args.src_ip,
                address[0],
                k,
                pkt_num,
                timestamp
            ))
