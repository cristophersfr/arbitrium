import logging
import tornado.ioloop
from tornado.web import Application, url

app = None

logger = logging.getLogger('simulation')


class MininetHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

class ControllerHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

class ApplicationHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

def make_app():
    return tornado.web.Application([
        url('/mininet', MainHandler),
    ])

def load_app():


def start():


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
