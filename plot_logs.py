import os
import pprint
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

ports = [12000, 12001, 12002, 12003]

rtt_mean = []
pdr_mean = []

for port in ports:
    print("Port {}".format(port))
    results = {
        'src_ip': [],
        'dst_ip': [],
        'flow_id': [],
        'packet_id': [],
        'timestamp': []
    }

    server_file = open("./logs/server_{0}.log".format(port), "r")
    client_file = open("./logs/client_{0}.log".format(port), "r")

    print("Reading server file")
    for recv in server_file:
        data = recv.split(" ")
        results['src_ip'].append(data[2])
        results['dst_ip'].append(data[0])
        results['flow_id'].append(data[3])
        results['packet_id'].append(data[4])
        results['timestamp'].append(float(data[5].strip()))

    print("Reading client file")
    for sent in client_file:
        data = sent.split(" ")
        results['src_ip'].append(data[0])
        results['dst_ip'].append(data[2])
        results['flow_id'].append(data[3])
        results['packet_id'].append(data[4])
        results['timestamp'].append(float(data[5].strip()))

    df = pd.DataFrame(data=results)

    grouped = df.groupby(['src_ip', 'dst_ip', 'flow_id', 'packet_id'])

    del df

    rtt = grouped['timestamp'].apply(lambda x: x.shift(-1) - x)

    del grouped

    packet_lost = rtt[pd.isna(rtt)]

    rtt = rtt[pd.isna(rtt) == False]

    packet_lost_rate = 1 - (len(rtt) / len(packet_lost))

    rtt_mean.append(rtt.mean())
    pdr_mean.append(packet_lost_rate)

print('Packet loss rate: {}'.format(packet_lost_rate))

print('Average Delay: {}'.format(average_rtt))
